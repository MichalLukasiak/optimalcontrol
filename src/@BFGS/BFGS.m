classdef BFGS < handle
    % Untitled Add summary here
    %
    % NOTE: When renaming the class name Untitled, the file name
    % and constructor name must be updated to use the class name.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object.
    
    properties
        % Public, tunable properties.
        B
        uOld
        gradOld
    end
    
    properties (Nontunable)
        % Public, non-tunable properties.
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    properties (DiscreteState)
    end
    
    methods
        % Constructor
        function obj = BFGS()

        end
        
        function du_BFGS = Calc_du(obj, u, grad, IndexSterowan)
            grad = grad';
            % first iteration
            if (isempty(obj.B))
                obj.B = eye(length(u));
                du_BFGS = grad;
                
                % saving data for 2nd iteration
                obj.uOld = cellfun(@(c) c(IndexSterowan), u);
                obj.gradOld = grad;
                return;
            end
            
            % not first iter
            d = cellfun(@(c) c(IndexSterowan), u) - obj.uOld;
            q = grad - obj.gradOld;
            obj.B = obj.B + obj.obliczMacierzKorekcyjnaBDFS( d, q, obj.B );
            du_BFGS = obj.B*grad;
            
            % saving data for next iter
            obj.uOld = cellfun(@(c) c(IndexSterowan), u);
            obj.gradOld = grad;
        end
        
        function [ dB ] = obliczMacierzKorekcyjnaBDFS(obj, d, q, B )
    % oblicza macierz korekcyjna dB
    % d = x(i+1) - x(i)
    % q = df(x(i+1)) - df(x(i))
    % B - przyblizony H^-1 w iteracji (i)
    dB = d*d'/(d'*q)*(1+q'*B*q/(d'*q)) - (B*q*d' + d*q'*B)/(d'*q);

end
    end
    
    methods (Access = protected)
        %% Common functions
        function setupImpl(obj,u)
            % Implement tasks that need to be performed only once,
            % such as pre-computed constants.
        end
        
        function y = stepImpl(obj,u)
            % Implement algorithm. Calculate y as a function of
            % input u and discrete states.
            y = u;
        end
        
        function resetImpl(obj)
            % Initialize discrete-state properties.
        end
        
        %% Backup/restore functions
        function s = saveObjectImpl(obj)
            % Save private, protected, or state properties in a
            % structure s.
        end
        
        function loadObjectImpl(obj,s,wasLocked)
            % Read private, protected, or state properties from
            % the structure s and assign it to the object obj.
        end
        
        %% Advanced functions
        function validateInputsImpl(obj,u)
            % Validate inputs to the step method at initialization.
        end
        
        function z = getDiscreteStateImpl(obj)
            % Return structure of states with field names as
            % DiscreteState properties.
            z = struct([]);
        end
        
        function processTunedPropertiesImpl(obj)
            % Define actions to perform when one or more tunable property
            % values change.
        end
        
        function flag = isInputSizeLockedImpl(obj,index)
            % Set true when the input size is allowed to change while the
            % system is running.
            flag = false;
        end
        
        function flag = isInactivePropertyImpl(obj,prop)
            % Implement logic for making public properties invisible based on
            % object configuration, for the command line and block dialog.
        end
    end
end

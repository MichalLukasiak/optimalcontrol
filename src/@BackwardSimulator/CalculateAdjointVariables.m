function [] = CalculateAdjointVariables(obj, forwardSimulator, objectiveFunction, aModel, q, v, M, Cq, A, B)
% oblicz p, w, mi dla przedzialu czasu idac od konca
% p,w,mi sa indeksowane od 1 -> do n
% p, w (1) wzieto z warunku brzegowego i jest dane
krok = forwardSimulator.Dt;
lWspolrzednych = aModel.CoordinatesAmount;
lWiezow = aModel.ConstraintsAmount;

dlugosci = aModel.BodiesLengths;

alfa = [1,       -1,        zeros(1,4); % wspolczynniki algorytmu BDF
        3/2,     -4/2,      1/2,    zeros(1,3);
        11/6,    -18/6,     9/6,    -2/6,    zeros(1,2);
        25/12,   -48/12,    36/12,  -16/12,  3/12,  zeros(1,1);
        137/60,  -300/60,   300/60, -200/60, 75/60, -12/60];


for i=1:forwardSimulator.SamplesAmount-1
    rank = min([i, 5]);
    tau = forwardSimulator.SamplesAmount-i;
    alfa0 = alfa(rank,1);

    
    hq = objectiveFunction.Gethq( aModel.ConsideredVariables, q{tau}, tau);
    hv = objectiveFunction.Gethv( aModel.ConsideredVariables, v{tau}, tau);
    
    W = alfa0^2*M{tau}+krok^2*A{tau}-alfa0*krok*B{tau};
    r = -alfa0*krok*hv' - krok*krok*hq' -krok*bdfSumVector1DoK(obj.p, lWspolrzednych, tau, rank, alfa, -1)-...
        alfa0*bdfSumMatrixXVector1DoK(M{tau}, obj.w, lWspolrzednych, tau, rank, alfa, -1);
    L = [W, krok^2*Cq{tau}';
        Cq{tau}, zeros(lWiezow, lWiezow)];
    P = [r;
        zeros(lWiezow,1)];
    X = L\P;
    obj.w{tau} = X(1:length(r));
    obj.mi{tau} = X(length(r)+1:end);
    obj.p{tau} = -krok/alfa0 * (hq' + A{tau}*obj.w{tau}+Cq{tau}'*obj.mi{tau})-1/alfa0*bdfSumVector1DoK(obj.p, lWspolrzednych, tau, rank, alfa, -1);
end
end

function sum = bdfSumVector1DoK(v, lWspolrzednych, idx, bdfRank, alfa, forward)
% v - array of cells
% dt - krok
% idx - indeks n-tej iteracji
% bdfRank - rzad metody bdf
% alfa - wspolczynniki bdf
% forward = 1 je�eli symulacja idzie wporzd (od 1 do n), -1 jezeli od konca
% (od n do 1)
sum = zeros(lWspolrzednych,1);
alfaIdx = 2;
for i=idx+1:-forward:idx-forward*bdfRank
    sum = sum + alfa(bdfRank, alfaIdx)*v{i};
    alfaIdx = alfaIdx + 1;
end
end

function sum = bdfSumMatrixXVector1DoK(mat, v, lWspolrzednych, idx, bdfRank, alfa, forward)
% v - array of cells
% dt - krok
% idx - indeks n-tej iteracji
% bdfRank - rzad metody bdf
% alfa - wspolczynniki bdf
% forward = 1 je�eli symulacja idzie wporzd (od 1 do n), -1 jezeli od konca
% (od n do 1)
sum = zeros(lWspolrzednych,1);
alfaIdx = 2;
for i=idx+1:-forward:idx-forward*bdfRank
    sum = sum + alfa(bdfRank, alfaIdx)*mat*v{i};
    alfaIdx = alfaIdx + 1;
end
end


function [  ] = CalculateAdjointVariablesAtBoundary(obj, objectiveFunction, aModel, q, v, M, Cq)
% oblicza p, w dla tau = 0, t = T
% Sv = obliczSv(v{end}); % pochodna czastowa po v kosztu koncowego
lWspolrzednych = aModel.CoordinatesAmount;
lWiezow = aModel.ConstraintsAmount;
Sv = objectiveFunction.GetSv(aModel.ConsideredVariables, v{end});
Sq = objectiveFunction.GetSq(aModel.ConsideredVariables, q{end}); % po q
% Sv = [10*2*v{end}(1), zeros(1,5)];
% Sq = [2*(q{end}(1)-0.1), zeros(1,5)];
Left = [M{end} Cq{end}';
    Cq{end} zeros(lWiezow, lWiezow) ];
Right = [-Sv'; zeros(lWiezow,1)];
x = Left\Right;
obj.w{end} = x(1:lWspolrzednych);
ksi = x(lWspolrzednych+1:end);
CqvqT = aModel.GetCqvqT( q{end}, v{end} ); % Cq * v, pochodna czastkowa po 

obj.p{end} = -CqvqT*ksi-Sq';
end


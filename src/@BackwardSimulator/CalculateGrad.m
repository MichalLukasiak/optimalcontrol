function [ grad ] = CalculateGrad( obj, forwardSimulator, objectiveFunction, aModel, u, q, v, M, Cq, A, B )
    %% oblicznie warunku p, w na brzegu

    obj.CalculateAdjointVariablesAtBoundary(objectiveFunction, aModel, q, v, M, Cq);
    obj.CalculateAdjointVariables(forwardSimulator, objectiveFunction, aModel, q, v, M, Cq, A, B);
    %% weryfikacja wynikow
%     sprawdzPoprawnoscObliczen( q, v, vp, Q, lam, C, Cq, M, w, p, mi, lProbek, log);
%     testCqlamqT;
%     testCqvqT;
    %% obliczenie gradientu
    [ huMx ] = objectiveFunction.Gethu( u, aModel.CoordinatesAmount, aModel.IndexSterowan);
    fu = aModel.Getfu();
    for i = 1:length(obj.w)
        wTfu(:,i) = obj.w{i}' * fu;
    end
    grad = huMx - wTfu;

end


function [ ] = AdaGrad(obj, grad, idxsCtrl )
% zmienia sterowanie o du zgodnie z z rownaniem 
obj.Iteration = obj.Iteration + 1;

%% aktualizacja sterowania
if isempty(obj.Optimizer)
    obj.Optimizer = AdaGrad();
end

du = obj.Optimizer.Optimize(obj.StepOld, grad);

obj.Krok(obj.Iteration) = norm(du);
for i=1:size(obj.u,1)
    for j = 1:length(idxsCtrl) % adds du to corresponding place in forces
        obj.u{i}(idxsCtrl(j)) = obj.u{i}(idxsCtrl(j)) + du(j, i);
    end
end

obj.uOld = obj.u;
obj.GradOld = grad;
obj.StepOld = du;
end 
function [u ] = CalculateU_plus_du(obj, du, idxsCtrl )

u = obj.u;
for i=1:size(obj.u,1)
    for j = 1:length(idxsCtrl) % adds du to corresponding place in forces
        u{i}(idxsCtrl(j)) = obj.u{i}(idxsCtrl(j)) + du(j, i);
    end
end

end 

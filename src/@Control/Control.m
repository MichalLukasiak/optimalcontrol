classdef Control < handle
    % Untitled Add summary here
    %
    % NOTE: When renaming the class name Untitled, the file name
    % and constructor name must be updated to use the class name.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object.
    
    properties
        % Public, tunable properties.
        GradNew;
        GradOld;
        u
        uOld;
        Iteration;
        StepOld;
        
        J;
        Percentage;
        duHistory;
        Krok;
        
        Optimizer;
    end
    
    properties (Nontunable)
        % Public, non-tunable properties.
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    properties (DiscreteState)
    end
    
    methods
        % Constructor
        function obj = Control(samplesAmount, coordsAmount)
            % Support name-value pair arguments when constructing the
            % object.
%             setProperties(obj,nargin,varargin{:});
            obj.u = cell(samplesAmount,1); obj.u(:) = {zeros(coordsAmount,1)};
            obj.Iteration = 1;
            
        end
        
        [ ] = CalculateNewControl(obj, grad );
        [  ] = ConstantStep(obj )
        [uNew, duNew] = OptimConstantDecrease(obj, grad, idxsCtrl, percentage );
        [du, step ] = Getdu_OptimConstantDecrease(obj, grad, idxsCtrl, percentage );
        [u ] = CalculateU(obj, du, idxsCtrl );
        [ ] = ChangeU(obj, du, idxsCtrl );
        [u ] = CalculateU_plus_du(obj, du, idxsCtrl );
        
        function [isObjFunSmaller] = CompareObjectiveFunction(obj, val)
            if (isempty(obj.J) || val <= obj.J(end)  )
                isObjFunSmaller = 1;
            else
                isObjFunSmaller = 0;
            end
        end
        
        function [] = ApplyNewObjectiveFunctionValue(obj, JNew)
            if ((length(obj.Iteration) > 1) && (JNew < obj.J(end)))
                obj.uBest = obj.u;
            end
            obj.J(obj.Iteration) = JNew;
            
        end
        
        function [isObjFunSmaller] = SaveObjectiveFunction(obj, val)
            obj.J(obj.Iteration) = val;
        end
        
        function [] = SetInitialControl(obj, ctrl, index)
            for i=1:length(obj.u)
                obj.u{i}(index) = ctrl(i);
            end
        end
    end
    
    methods (Access = protected)
        %% Common functions
        function setupImpl(obj,u)
            % Implement tasks that need to be performed only once,
            % such as pre-computed constants.
        end
        
        function y = stepImpl(obj,u)
            % Implement algorithm. Calculate y as a function of
            % input u and discrete states.
            y = u;
        end
        
        function resetImpl(obj)
            % Initialize discrete-state properties.
        end
        
        %% Backup/restore functions
        function s = saveObjectImpl(obj)
            % Save private, protected, or state properties in a
            % structure s.
        end
        
        function loadObjectImpl(obj,s,wasLocked)
            % Read private, protected, or state properties from
            % the structure s and assign it to the object obj.
        end
        
        %% Advanced functions
        function validateInputsImpl(obj,u)
            % Validate inputs to the step method at initialization.
        end
        
        function z = getDiscreteStateImpl(obj)
            % Return structure of states with field names as
            % DiscreteState properties.
            z = struct([]);
        end
        
        function processTunedPropertiesImpl(obj)
            % Define actions to perform when one or more tunable property
            % values change.
        end
        
        function flag = isInputSizeLockedImpl(obj,index)
            % Set true when the input size is allowed to change while the
            % system is running.
            flag = false;
        end
        
        function flag = isInactivePropertyImpl(obj,prop)
            % Implement logic for making public properties invisible based on
            % object configuration, for the command line and block dialog.
        end
    end
end

function [du, step ] = Getdu_OptimConstantDecrease(obj, grad, idxsCtrl, percentage )
% get control u if objective function would decrease by "percentage"
%% aktualizacja sterowania

calka = norm(grad)^2;
if ~isempty(obj.J)
    step = percentage*abs(obj.J(end))/calka/100;
else
    step = percentage*10/calka;
end

du = - step * grad;
end 


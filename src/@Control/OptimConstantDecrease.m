function [uNew, duNew ] = OptimConstantDecrease(obj, grad, idxsCtrl, percentage )
% zmienia sterowanie o du zgodnie z z rownaniem 
if(isempty(obj.Percentage))
    obj.Percentage = percentage;
end

% % wyznaczanie wzmocnienia
if length(obj.J) > 21
if ( obj.J(end)> obj.J(end-1))
obj.Percentage = obj.Percentage*0.93; % zmniejszenie kroku jesli f celu zwiekszyla sie
else if (JMaleje(obj.J, 20))
       obj.Percentage = obj.Percentage*1.01;
    end
end
end

% J_wspSter = strcat('J = ', num2str(J(end)), ', wsp. sterowania = ', num2str(wspSterowania), '\n');
% fprintf(J_wspSter); % wypisanie wartosci f. celu i wspolczynnika sterowania
% fprintf(logTemp, 'krok = %d', k);
% fprintf('krok = %f \n', k);

%% aktualizacja sterowania
calka = (sum(grad)*0.001)^2;
if isempty(obj.J)
    k = 0;
else
    k = obj.Percentage*abs(obj.J(end))/calka/100;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


du = - k * grad;

% if (norm(du)>1000)
%     du = 300*du/norm(du);
% end
obj.Krok(obj.Iteration) = norm(du);

uNew = obj.u;
for i=1:size(obj.u,1)
    for j = 1:length(idxsCtrl) % adds du to corresponding place in forces
        uNew{i}(idxsCtrl(j)) = obj.u{i}(idxsCtrl(j)) + du(j, i);
    end
end
duNew = du;

end 

function zbytMalyKrok = JMaleje(J, lIter)
zbytMalyKrok = 1;
for i=0:lIter-1
    if( J(end-i) > J(end-i-1))
        zbytMalyKrok = 0;
        return;
    end
end
end

function [uNew, duNew  ] = OptimConstantStep(obj, grad, idxsCtrl, step )
% zmienia sterowanie o du zgodnie z z rownaniem 
% obj.Iteration = obj.Iteration + 1;


%% aktualizacja sterowania
du = - grad * step/norm(grad);
obj.Krok(obj.Iteration) = step;
uNew = obj.u;
for i=1:size(obj.u,1)
    for j = 1:length(idxsCtrl) % adds du to corresponding place in forces
        uNew{i}(idxsCtrl(j)) = obj.u{i}(idxsCtrl(j)) + du(j, i);
    end
end


duNew = du;
end 

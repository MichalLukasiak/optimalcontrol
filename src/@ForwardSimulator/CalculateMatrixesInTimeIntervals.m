function [q, v, vp, M, C, Cq, Q, Gamma, A, B, lambda] = CalculateMatrixesInTimeIntervals( obj, aModel, u)
% olbiczam macierze i wkladam jest w array of cells
dlugosci = aModel.BodiesLengths;
tlumienie = aModel.Dumpings;
masy = aModel.BodiesMasses;
alf = 5; bet = 5;
lWspolrzednych = aModel.CoordinatesAmount;
for i=1:obj.SamplesAmount
    q{i} = obj.SimResults(i,1:lWspolrzednych)';
    v{i} = obj.SimResults(i,lWspolrzednych+1:end)';
    aModel.SetState(q{i}, v{i});
    C{i} = aModel.GetC();
    M{i} = aModel.GetM();
    Cq{i} = aModel.GetCq();
    Q{i} = aModel.GetQ(u{i});
    Gamma{i} = aModel.GetGamma();
    Left = [aModel.GetM() Cq{i}';
        Cq{i} zeros(aModel.ConstraintsAmount, aModel.ConstraintsAmount)];
    Right =  [Q{i};
              Gamma{i}-2*alf*Cq{i}*v{i}-bet^2*C{i}];
    solution = Left\Right;
    vp{i} = solution(1:lWspolrzednych);
    lambda{i} = solution(lWspolrzednych+1:end);
    A{i} = aModel.GetA(lambda{i});
    B{i} = aModel.GetB();
end
end


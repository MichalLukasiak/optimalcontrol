classdef ForwardSimulator < matlab.System
    % Untitled11 Add summary here
    %
    % NOTE: When renaming the class name Untitled11, the file name
    % and constructor name must be updated to use the class name.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object.
    
    properties
        % Public, tunable properties.
        TStart;
        TEnd;
        Dt;
        SamplesAmount
        CoordinatesAmount;
        ConstraintsAmount;
        
        C;
        Cq;
        A;
        B;
        q;
        v;
        vp;
        Q;
        Gamma;
        lambda;
        M;
        
        SimTime;
        SimResults;
    end
    
    properties (Nontunable)
        % Public, non-tunable properties.
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    properties (DiscreteState)
    end
    
    methods
        % Constructor
        function obj = ForwardSimulator(t0, tEnd, dt)
            % Support name-value pair arguments when constructing the
            % object.
%             setProperties(obj,nargin,varargin{:});
            obj.TStart = t0;
            obj.TEnd = tEnd;
            obj.Dt = dt;
            obj.SamplesAmount = int16(obj.TEnd/obj.Dt) + 1;
        end
        
        [q, v] = Simulate(obj, model, u);
        [q, v, lam] = SimulateGGL(obj, model, u );
        [q, v] = SimulateDirectTranscription(obj, model, u);
        
    end
    
    methods(Abstract)
        
    end
    
    methods (Access = protected)
        %% Common functions
%         function setupImpl(obj,u)
%             % Implement tasks that need to be performed only once,
%             % such as pre-computed constants.
%         end
%         
%         function y = stepImpl(obj,u)
%             % Implement algorithm. Calculate y as a function of
%             % input u and discrete states.
%             y = u;
%         end
%         
%         function resetImpl(obj)
%             % Initialize discrete-state properties.
%         end
        
%         %% Backup/restore functions
%         function s = saveObjectImpl(obj)
%             % Save private, protected, or state properties in a
%             % structure s.
%         end
%         
%         function loadObjectImpl(obj,s,wasLocked)
%             % Read private, protected, or state properties from
%             % the structure s and assign it to the object obj.
%         end
        
%         %% Advanced functions
%         function validateInputsImpl(obj,u)
%             % Validate inputs to the step method at initialization.
%         end
%         
%         function z = getDiscreteStateImpl(obj)
%             % Return structure of states with field names as
%             % DiscreteState properties.
% %             z = struct([]);
%         end
%         
%         function processTunedPropertiesImpl(obj)
%             % Define actions to perform when one or more tunable property
%             % values change.
%         end
        
%         function flag = isInputSizeLockedImpl(obj,index)
%             % Set true when the input size is allowed to change while the
%             % system is running.
%             flag = false;
%         end
%         
%         function flag = isInactivePropertyImpl(obj,prop)
%             % Implement logic for making public properties invisible based on
%             % object configuration, for the command line and block dialog.
%         end
    end
end

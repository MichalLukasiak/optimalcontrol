function [q, v, vp,  C, A, lambda] = ReserveMemory( obj )
%RESERVEMEMORY Summary of this function goes here
%   Detailed explanation goes here

        C= cell(obj.SamplesAmount,1); C(:) = {zeros(obj.ConstraintsAmount,1)};
%         Cq;
        A = cell(obj.SamplesAmount,1); A(:) = {zeros(obj.CoordinatesAmount,obj.CoordinatesAmount)};
%         obj.B = cell(obj.SamplesAmount,1); obj.B(:) = {zeros(obj.CoordinatesAmount,obj.CoordinatesAmount)};
        q = cell(obj.SamplesAmount,1); q(:) = {zeros(obj.CoordinatesAmount,1)};
        v = cell(obj.SamplesAmount,1); v(:) = {zeros(obj.CoordinatesAmount,1)};
        vp = cell(obj.SamplesAmount,1); vp(:) = {zeros(obj.CoordinatesAmount,1)};
        lambda = cell(obj.SamplesAmount,1); lambda(:) = {zeros(obj.CoordinatesAmount,1)};

end


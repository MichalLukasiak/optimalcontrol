function [q, v] = Simulate(obj, model, u )
model.SetInitialConditions();
obj.CoordinatesAmount = model.CoordinatesAmount;
obj.ConstraintsAmount = model.ConstraintsAmount;
tSpan = obj.TStart:obj.Dt:obj.TEnd;
Y0 = [model.q; model.v]; % initial conditions
options = odeset('AbsTol', 1e-6);
[t, Y ] = ode45(@(t, Y)  RHS(t, obj.Dt, Y, model, u), tSpan, Y0, options);
obj.SimTime = t;
obj.SimResults = Y;

for i=1:length(Y)
    q{i} = Y(i,1:obj.CoordinatesAmount)';
    v{i} = Y(i,obj.CoordinatesAmount+1:end)';
end
end

%  RHS(t, Y, u, M, dt, dlugosci, tlumienie, masy)
 
function [dY] = RHS(t, dt, Y, model, u)
alf = 5; bet = 5;
coordsAmount  = model.CoordinatesAmount;
model.q = Y(1:model.CoordinatesAmount);
model.v = Y(model.CoordinatesAmount+1:model.CoordinatesAmount*2);
C = model.GetC();
Cq = model.GetCq();
idx = int16(t/dt+1);
Q = model.GetQ(u{idx});
Gamma = model.GetGamma();
% A = sparse([M, Cq';
%     Cq, zeros(size(Cq,1), size(Cq,1))]);

A = [model.GetM(), Cq';
    Cq, zeros(size(Cq,1), size(Cq,1))]; % not sparse
b = [Q; Gamma-2*alf*Cq*model.v-bet^2*C];
% b = [Q; Gamma];
x=A\b;

dY = [  model.v;
        x(1:coordsAmount,1)];

end

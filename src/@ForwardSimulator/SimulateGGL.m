function [q, v, lam] = SimulateGGL(obj, model, u )
model.SetInitialConditions();
obj.CoordinatesAmount = model.CoordinatesAmount;
obj.ConstraintsAmount = model.ConstraintsAmount;
tSpan = obj.TStart:obj.Dt:obj.TEnd;
x0 = [model.q; model.v; 0; 0]; % initial conditions
dx0 = [model.v; zeros(3,1); 9.80665; 22; 0]; % moze do poprawy !!!!!!!!!
options = odeset('AbsTol', 1e-6);
[t, Y ] = ode15i(@(t, x, dx)  RHS(t, x, dx, obj.Dt, model, u), tSpan, x0, dx0, options);
obj.SimTime = t;
obj.SimResults = Y;

for i=1:length(Y)
    q{i} = Y(i,1:obj.CoordinatesAmount)';
    v{i} = Y(i,obj.CoordinatesAmount+1:2*obj.CoordinatesAmount)';
    lam{i} = Y(i, 2*obj.CoordinatesAmount+1 : 2*obj.CoordinatesAmount + 2)';
end
end

%  RHS(t, Y, u, M, dt, dlugosci, tlumienie, masy)
 
function [F] = RHS(t, x, dx, dt, model, u)
idx = int16(t/dt+1);
masses = model.BodiesMasses;
g = 9.80665;
F = [...
-dx(1) + x(5) - x(10)*(x(1) - x(3))/masses(1);
-dx(2) + x(6) + x(10)*x(2)/masses(2);
-dx(3) + x(7) - x(10)*(x(3) - x(1))/masses(3);
-dx(4) + x(8) - x(10)*x(4)/masses(3); 
-dx(5) + u{idx}(1)/masses(1) - x(9)*(x(1)-x(3))/masses(1);
-dx(6) - u{idx}(2)/masses(2) + x(9)*x(2)/masses(2); % minus?
-dx(7) - x(9)*(x(3) - x(1))/masses(3);
-dx(8) + g - x(9)*x(4)/masses(3);
(x(3) - x(1))^2 - x(2)^2 + x(4)^2;
(x(3) - x(1))*(x(7) - x(5)) - x(2)*x(6) + x(4)*x(8);...
];
end

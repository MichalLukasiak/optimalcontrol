function [q, v] = SimulateGGL_ode45(obj, model, u )
model.SetInitialConditions();
obj.CoordinatesAmount = model.CoordinatesAmount;
obj.ConstraintsAmount = model.ConstraintsAmount;
tSpan = obj.TStart:obj.Dt:obj.TEnd;
x0 = [model.q; model.v]; % initial conditions

options = odeset('AbsTol', 1e-6, 'MaxStep', 0.005);
[t, Y ] = ode45(@(t, x)  RHS(t, x, obj.Dt, model, u), tSpan, x0, options);
obj.SimTime = t;
obj.SimResults = Y;

for i=1:length(Y)
    q{i} = Y(i,1:obj.CoordinatesAmount)';
    v{i} = Y(i,obj.CoordinatesAmount+1:2*obj.CoordinatesAmount)';
end
end

%  RHS(t, Y, u, M, dt, dlugosci, tlumienie, masy)
 
function [dY] = RHS(t, x, dt, model, u)
idx = int16(t/dt+1);

model.q = x(1:4);
model.v = x(5:8);
M = model.GetM();
Cq = model.GetCq();
Q = model.GetQ(u{idx});
Gamma = model.GetGamma();

L = [...
    M           zeros(4,4)  Cq'         zeros(4,1)  ;
    zeros(4,4)  M           zeros(4,1)  Cq'         ;
    Cq          zeros(1,4)  0           0           ;
    zeros(1,4)           Cq          0           0           ];

R = [...
    M*model.v;
    Q;
    0
    Gamma];
X = L\R;

dY = X(1:8);
end

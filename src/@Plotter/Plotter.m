classdef Plotter < handle
    % Untitled2 Add summary here
    %
    % NOTE: When renaming the class name Untitled2, the file name
    % and constructor name must be updated to use the class name.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object.
    
    properties
        % Public, tunable properties.
        Button;
        StopCondition;
        ControlFigure;
        ObjectiveFunctionFigure;
        LIteracji
        ControlsAmount;
    end
    
    properties (Nontunable)
        % Public, non-tunable properties.
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    properties (DiscreteState)
    end
    
    methods
        % Constructor
        function obj = Plotter(controlsAmount)
            % Support name-value pair arguments when constructing the
            % object.
            %             setProperties(obj,nargin,varargin{:});

        end
    end
    
    methods
        function [] = CreateControlPlot(obj)
            obj.LIteracji = 0;
            obj.ControlFigure = figure;
            obj.StopCondition = 1;
            obj.Button = uicontrol('Style', 'PushButton', ...
                'String', 'Break', ...
                'Callback', 'Plotter.StopCondition = 0;');
            hold on;
            grid on;
        end
        function [] = PlotControl(obj, u, indexsControl, tK, dt)
            obj.LIteracji = obj.LIteracji + 1;
            col = min(obj.LIteracji/1000, 1);
            figure(obj.ControlFigure);
            for i = 1:length(indexsControl)
                subplot(length(indexsControl), 1, i)
                res = cellfun(@(c) c(indexsControl(i)), u);
                plot(0:dt:tK, res, 'color', [1-col, col, 0]);
                hold on; grid on;
                xlabel('czas [s]');
                if (i==1)
                    ylabel('F [N] - wozek');
                else 
                    ylabel('F [N] - korba');    
                end
            end

            pause(0.1);
        end
        
        function [] = PlotObjectiveFunction(obj, J)
            figure(obj.ObjectiveFunctionFigure);
            semilogy(J); grid on;
                        xlabel('iteracja');
            ylabel('wartosc funkcji celu');

        end
        
        function [] = CreateObjectiveFunctionFigure(obj)
            obj.ObjectiveFunctionFigure = figure();

        end
        
        % rysowanie wynikow
        function h = rysujZestawienieWykresow_Pendulum(obj, q, v, control, forwardSimulator, objectiveFunction, lCzlonow)
            %% zbieznosc i sterowanie
            lProbek = length(q);
            fPodcalkowa = objectiveFunction.fPodcalkowa;
            fKoncowa = objectiveFunction.fKoncowa;
            u = control.u;
            
            tK = forwardSimulator.TEnd;
            dt = forwardSimulator.Dt;
            krok = control.Krok;
            J = control.J;
            
            h = figure;
            subplot(lCzlonow+1,2,1)
            [hAx,hLine1,hLine2] = plotyy(1:length(J), J, 1:length(krok), krok, @semilogy, @plot); grid on;
            ylabel(hAx(1), 'funkcja celu');
            ylabel(hAx(2), 'krok');
            xlabel('probka');
            
            subplot(lCzlonow+1,2,2)
            Plotter.rysuj_zest(u, 1, tK, dt); grid on;
            xlabel('czas [s]')
            ylabel('u [N]');
            
            %% wozek
            subplot(lCzlonow+1,2,3)
            Plotter.rysuj_zest(q,1, tK, dt); hold on; grid on;
            Plotter.rysujWartoscZadanaIWartoscKoncowa(1, lProbek, fPodcalkowa.q, fKoncowa.q, tK, dt)
            
            xlabel('czas [s]')
            ylabel('polozenie wozka [m]');
            
            subplot(lCzlonow+1,2,4)
            Plotter.rysuj_zest(v,1, tK, dt); hold on; grid on;
            Plotter.rysujWartoscZadanaIWartoscKoncowa(1, lProbek, fPodcalkowa.v, fKoncowa.v, tK, dt)
            xlabel('czas [s]')
            ylabel('predkosc wozka [m/s]');
            
            %% wahadla
            for i=2:lCzlonow
                subplot(lCzlonow+1,2,2*i+1)
                Plotter.rysuj_zest(q,3*i, tK, dt); hold on; grid on;
                Plotter.rysujWartoscZadanaIWartoscKoncowa(i, lProbek, fPodcalkowa.q, fKoncowa.q, tK, dt)
                xlabel('czas [s]')
                ylabel(strcat('\phi ', num2str(i), ' [rad]'));
                
                subplot(lCzlonow+1,2,2*i+2)
                Plotter.rysuj_zest(v,3*i, tK, dt);hold on; grid on;
                Plotter.rysujWartoscZadanaIWartoscKoncowa(i, lProbek, fPodcalkowa.v, fKoncowa.v, tK, dt)
                xlabel('czas [s]')
                ylabel(strcat('\omega ', num2str(i), ' [rad/s]'));
            end
            
%             suptitle(strcat('Symulacja do tk= ', num2str(tk), ' dt = ', num2str(dt),  ' q0 = ' , vect2str(q{1}), ' q''0 = 0') )
        end
        
        
        
        
        
    end
    
    methods (Static)
        function [] = rysujWartoscZadanaIWartoscKoncowa(nrCzlonu, lProbek, fPodcalkowaWspolrzedna, fKoncowaWspolrzedna, tK, dt)
            if( fPodcalkowaWspolrzedna.wsp(nrCzlonu) ~= 0)
                if ( length(fPodcalkowaWspolrzedna.zad{nrCzlonu}) == 1)
                    plot([0, lProbek*dt], [fPodcalkowaWspolrzedna.zad{nrCzlonu}(1), fPodcalkowaWspolrzedna.zad{nrCzlonu}(1)], 'k');
                else
                    plot(0:dt:tK, fPodcalkowaWspolrzedna.zad{nrCzlonu}, 'k', '--');
                end
            end
            if( fKoncowaWspolrzedna.wsp(nrCzlonu) ~= 0)
                scatter(lProbek*dt, fKoncowaWspolrzedna.zad(nrCzlonu), 'k');
            end
        end
        
        function [ ] = rysuj(cellOfVectors, nrWspolrzednej )
            lIteracji = 1000;
            col = min(lIteracji/1000, 1);
            res = cellfun(@(c) c(nrWspolrzednej), cellOfVectors);
            plot(res, 'color', [1-col, col, 0]);
        end
        
        function [ ] = rysuj_zest(cellOfVectors, nrWspolrzednej, tK, dt )
            res = cellfun(@(c) c(nrWspolrzednej), cellOfVectors);
            plot(0:dt:tK, res, 'color', 'red');
        end
%         s = vect2str(v,varargin)
    end
    
    methods (Access = protected)
        %% Common functions
        function setupImpl(obj,u)
            % Implement tasks that need to be performed only once,
            % such as pre-computed constants.
        end
        
        function y = stepImpl(obj,u)
            % Implement algorithm. Calculate y as a function of
            % input u and discrete states.
            y = u;
        end
        
        function resetImpl(obj)
            % Initialize discrete-state properties.
        end
        
        %% Backup/restore functions
        function s = saveObjectImpl(obj)
            % Save private, protected, or state properties in a
            % structure s.
        end
        
        function loadObjectImpl(obj,s,wasLocked)
            % Read private, protected, or state properties from
            % the structure s and assign it to the object obj.
        end
        
        %% Advanced functions
        function validateInputsImpl(obj,u)
            % Validate inputs to the step method at initialization.
        end
        
        function z = getDiscreteStateImpl(obj)
            % Return structure of states with field names as
            % DiscreteState properties.
            z = struct([]);
        end
        
        function processTunedPropertiesImpl(obj)
            % Define actions to perform when one or more tunable property
            % values change.
        end
        
        function flag = isInputSizeLockedImpl(obj,index)
            % Set true when the input size is allowed to change while the
            % system is running.
            flag = false;
        end
        
        function flag = isInactivePropertyImpl(obj,prop)
            % Implement logic for making public properties invisible based on
            % object configuration, for the command line and block dialog.
        end
    end
end

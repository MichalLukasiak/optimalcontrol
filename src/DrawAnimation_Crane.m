function [  ] = DrawAnimation_Crane(q, tK, dt, nRows, nCols, BodiesLengths, BodiesAmount, objectiveFunction)
%% init
subplot = @(m,n,p) subtightplot (m, n, p, [0.01 0.01], [0.01 0.01], [0.01 0.01]);
lProbek = length(q);
subplotAmount = nRows*nCols;

%% create main window
mainFig = figure();
set(mainFig, 'PaperPositionMode','auto');
limits = FindAxisLimits(q);
xSpan = limits(2) - limits(1);
ySpan = limits(4) - limits(3);

figSizeScale = 900;
xlength = figSizeScale*xSpan*nCols/(ySpan*nRows);
ylength = figSizeScale;
set(mainFig, 'Position', [50 50 xlength ylength])

for row = 1:nRows
    for col = 1:nCols
        subplotIdx = (row-1)*nCols+col;
        [qInTime, time] = get_qInTime(subplotIdx, subplotAmount, q, tK, lProbek);
        DrawSnap(nRows, nCols, subplotIdx, qInTime, limits, BodiesAmount, BodiesLengths, time, objectiveFunction);


    end

end
end
function [] = DrawSnap(row, col, subplotIdx, qInTime, limits, BodiesAmount, BodiesLengths, time, objectiveFunction)
subplot = @(m,n,p) subtightplot (m, n, p, [0.01 0.01], [0.01 0.01], [0.01 0.01]);
    sp = subplot(row, col, subplotIdx);
    wozek = plot([0,0], [0,0]); hold on;
    wozek.LineWidth = 3;
    wozek.Color = 'b';
    plot([limits(1), limits(2)], [0,0], 'k'); % rail

    rope = plot([0, 0], [0, 0]);
    rope.LineWidth =3;
    rope.Color = [0, 1, 0];

    
    wozek.XData = [qInTime(1)-0.1; qInTime(1)+0.1];
 
    rope.XData = [qInTime(1), qInTime(3)];
    rope.YData = [0, -qInTime(4)];
    
    scatter(qInTime(3), -qInTime(4), 'filled', 'k');
    
    %% drawing trajectory
    xTraj = objectiveFunction.fPodcalkowa.q.zad{1};
    yTraj = objectiveFunction.fPodcalkowa.q.zad{2};
    plot(xTraj, -yTraj, 'k--');


    axis (limits)
    set(sp, 'box', 'on', 'Visible', 'on', 'xtick', [], 'ytick', [])
    timeText = text(0.98, 0.02, strcat(num2str(time, '%1.3f'), ' s'), 'Units', 'Normalized', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'right', 'FontSize', 16);
    
end

function limits = FindAxisLimits(q)
qMx = cell2mat(q);
xMin = min(qMx(1,:))-0.1;
xMax = max(qMx(1,:))+0.1;
yMin = 0;
yMax = 0;

xMin = min(xMin, min(qMx(3,:)));
xMax = max(xMax, max(qMx(3,:)));
yMin = min(yMin, min(qMx(4,:)));
yMax = max(yMax, max(qMx(4,:)));

limits = [xMin-0.05 xMax+0.05 yMin+0.05 yMax+0.05];
% changing to crane - y with minus
limits = [limits(1:2), - limits(4), limits(3)];
end

function [qInTime, time] = get_qInTime(subplotIdx, subplotAmount, q, tK, samplesAmount)
    frac = (subplotIdx-1)/(subplotAmount-1);
    sampleNr = int16(frac*samplesAmount);
    if (sampleNr == 0) sampleNr = 1; end
    time = frac*tK;
    qInTime = q{sampleNr};
end

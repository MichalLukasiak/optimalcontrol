function [ grad ] = BackwardSimulatorKelly( q, v, lam, dt, tEnd, model, xZad, zZad, dxZad, dzZad )
% operujemy na zmiennej tau = tend - t
x=cellfun(@vertcat, q, v, 'UniformOutput', 0 );
% xZad = 
% zZad = 
dxZad = zeros(length(q), 1);
dzZad = zeros(length(q), 1);
p0 = zeros(10, 1); % to change
dp0 = zeros(10, 1);


tauSpan = 0:dt:tEnd;
% x0 = [model.q; model.v; 0; 0]; % initial conditions
% dx0 = [model.v; zeros(3,1); 9.80665; 22; 0]; % moze do poprawy !!!!!!!!!
% options = odeset('AbsTol', 1e-6);

[t, Y ] = ode15i(@(tau, p, dp)  RHS(tau, p, dp, x, xZad, zZad, dxZad, dzZad, dt, tEnd, model, lam), tauSpan, p0, dp0);

Y = flipud(Y);

grad = [Y(:,5)'/model.BodiesMasses(1); Y(:,6)'/model.BodiesMasses(2)];
grad = grad/norm(grad);
end

%  RHS(t, Y, u, M, dt, dlugosci, tlumienie, masy)
 
function [F] = RHS(tau, p, dp, x, xZad, zZad, dxZad, dzZad, dt, tEnd, model, lam)
time = tEnd - tau;
idx = int16((time)/dt+1);
m = model.BodiesMasses;
beta = 0;
F1 = dp(1) + lam{idx}(2)*p(1)/m(1) -lam{idx}(2)*p(3)/m(3) + lam{idx}(1)*p(5)/m(1) - lam{idx}(1)*p(7)/m(3) + 2*p(9)*(x{idx}(3) - x{idx}(1)) + p(10)*(x{idx}(7) -x{idx}(5));
F2 = dp(2) - lam{idx}(2)*p(2)/m(2) - lam{idx}(1)*p(6)/m(2) + 2*p(9)*x{idx}(2) + 2*p(10)*x{idx}(6);
F3 = dp(3) + xZad(idx) - x{idx}(3) - lam{idx}(2)*p(1)/m(1) + lam{idx}(2)*p(3)/m(3) - lam{idx}(1)*p(5)/m(1) + lam{idx}(1)*p(7)/m(3) - 2*p(9)*(x{idx}(3) - x{idx}(1)) - p(10)*(x{idx}(7) - x{idx}(5));
F4 = dp(4) + zZad(idx) - x{idx}(4) + lam{idx}(2)*p(4)/m(3) + lam{idx}(1)*p(8)/m(3) -2*p(9)*x{idx}(4) - p(10)*x{idx}(8);
F5 = dp(5) - p(1) + p(10)*(x{idx}(3) - x{idx}(1));
F6 = dp(6) - p(2) + p(10)*x{idx}(2);
F7 = dp(7) + beta*(dxZad(idx) - x{idx}(7)) - p(3) - p(10)*(x{idx}(3) - x{idx}(1));
F8 = dp(8) + beta*(dzZad(idx) - x{idx}(8)) - p(4) - p(10)*x{idx}(4);...
F9 = -(x{idx}(1) - x{idx}(3))*p(5)/m(1) + x{idx}(2)*p(6)/m(2) - (x{idx}(3) - x{idx}(1))*p(7)/m(3) - x{idx}(4)*p(8)/m(3);
F10 = -(x{idx}(1) - x{idx}(3))*p(1)/m(1) + x{idx}(2)*p(2)/m(2) - (x{idx}(3) - x{idx}(1))*p(3)/m(3) - x{idx}(4)*p(4)/m(3);
F = [...
F1; F2; F3; F4; F5; F6; F7; F8; F9; F10];

end
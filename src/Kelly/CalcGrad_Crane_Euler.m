function [ grad ] = CalcGrad_Crane_Euler( x, lambda, forwardSimulator, objectiveFunction, model )
    %% oblicznie p, mu na brzegu

    [ p_end ] = Calc_p_end( x{end}, objectiveFunction, model );
	[ p ] = Crane_euler_backward( forwardSimulator, objectiveFunction, model, x, lambda, p_end ); % returns p (in time - flipped)
    
    grad = p(5:6,:);
end


function [ p_end ] = Calc_p_end( xEnd, objectiveFunction, model )
% calc p at time t = Tend
Sxf = objectiveFunction.GetS_x(xEnd);
Df = model.GetFI_x(xEnd);
Cf = model.GetFLambda(xEnd, model.BodiesMasses);
p_end = Sxf' - Df'*(Cf'*Df')^-1*Cf'*Sxf';
end


function [lam] = CalculateMatrixesInTimeIntervals_Crane(q, v, model, u)
% olbiczam macierze i wkladam jest w array of cells

 M = model.GetM();
for i=1:length(q)
    % init model
    model.q = q{i};
    model.v = v{i};
    
    % calc matrixes
    Cq = model.GetCq();
    Q = model.GetQ(u{i});
    Gamma = model.GetGamma();
    
    L = [...
    M           zeros(4,4)  Cq'         zeros(4,1)  ;
    zeros(4,4)  M           zeros(4,1)  Cq'         ;
    Cq          zeros(1,4)  0           0           ;
    zeros(1,4)           Cq          0           0           ];

    R = [...
        M*model.v;
        Q;
        0
        Gamma];
    
    X = L\R;
    lam{i} = flip(X(9:10));
end
end


function [ err ] = CheckKellyForwardSimulationResults( qKelly, qAdjoint, vKelly, vAdjoint )
% checks if result of formward simulation using GGL formutalion is the same
% as M. Wojtyra attempt

zAdj = cellfun(@(c) c(4), qAdjoint);
zKelly = cellfun(@(c) c(4), qKelly);
qErr = cellfun(@minus,qKelly,qAdjoint,'Un',0);
qErrNorm = cellfun(@norm, qErr);
err = sum(qErrNorm);


end


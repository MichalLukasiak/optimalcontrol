function [ p ] = Crane_euler_backward( forwardSimulator, objectiveFunction, model, x, lambda, pStart )
% oblicza p, mu uzywajac implicit euler
% p(1) z warunku brzegowego
% uzywa sie rownan p(tau), gdzie tau = T - t, a na koncu flip'uje sie
% wektor
dt = forwardSimulator.Dt;
p(1:8,1) = pStart;


for tau=2:forwardSimulator.SamplesAmount
    t_idx = forwardSimulator.SamplesAmount-tau+1; % index w chwili t
   
    h_x = objectiveFunction.Geth_x(x{t_idx} , t_idx);
   
    f_x = model.Getf_x(lambda{t_idx}, model.BodiesMasses);
    FI_x = model.GetFI_x(x{t_idx});
    f_lam = model.GetFLambda( x{t_idx}, model.BodiesMasses);
    
    
    L = [eye(8) - dt*f_x', -dt*FI_x';
        f_lam',  zeros(2,2)];
    P = [p(1:8,tau-1) + dt*h_x';
        zeros(2,1)];
    X = L\P;
    p(1:8,tau) = X(1:8);
    mu(1:2,tau) = X(9:10);
end
p = fliplr(p);

end


function [  ] = SolveDAE_crane_kelly_GGL(q, v, lam, xZad, zZad, dt, Tend, model)
% rozwiazuje uklad DAE w postaci [P] = f(P, x, lam)

x=cellfun(@vertcat, q, v, 'UniformOutput', 0 );
% xZad = 
% zZad = 
dxZad = zeros(length(q), 1);
dzZad = zeros(length(q), 1);
p0 = zeros(10, 1); % to change
dp0 = zeros(10, 1);


tauSpan = 0:dt:Tend;

[y0mod,yp0mod] = decic(@(tau, p, dp) craneDAE_ode15i(tau, p, dp, x, lam, xZad, zZad, dxZad, dzZad,  model.BodiesMasses, dt, Tend),...
    0,p0, [ones(8,1); 0;0], dp0, zeros(10,1))
[t, Y ] = ode15i(@(tau, p, dp)  craneDAE_ode15i(tau, p, dp, x, lam, xZad, zZad, dxZad, dzZad, model.BodiesMasses, dt, Tend), tauSpan, y0mod, yp0mod);
end

function F = craneDAE_ode15i(tau, pVal, dpVal, xCells, lamCells, xZad, zZad, dxZad, dzZad,  masses, dt, Tend)
display(tau);
syms x1 x2 x3 x4 x5 x6 x7 x8 l1 l2 u1 u2 m mr mt g
syms p1 p2 p3 p4 p5 p6 p7 p8 mu1 mu2
syms dp1 dp2 dp3 dp4 dp5 dp6 dp7 dp8
syms xZads zZads dxZads dzZads
p = [p1; p2; p3; p4; p5; p6; p7; p8];
dp = [dp1; dp2; dp3; dp4; dp5; dp6; dp7; dp8];
mu = [mu1; mu2];
% m = 100; mr = 10; mt = 10; g = 9.81;

f=[...
    x5-l2*(x1-x3)/mt;
    x6+l2*x2/mr;   
    x7-l2*(x3-x1)/m;
    x8-l2*x4/m;
    u1/mt-l1*(x1-x3)/mt;
    -u2/mr+l1*x2/mr;
    -l1*(x3-x1)/m;
    g-l1*x4/m...
    ];

FI = [...
    (x3-x1)^2 - x2^2 + x4^2;
    (x3-x1)*(x7-x5) - x2*x6 + x4*x8...
    ];

h = 0.5*(x3 - xZads)*(x3 - xZads) + 0.5*(x4 - zZads)*(x4 - zZads);
gradh_x = [diff(h, x1), diff(h, x2), diff(h, x3), diff(h, x4), diff(h, x5), diff(h, x6), diff(h, x7), diff(h, x8)];

gradf_x = [diff(f, x1), diff(f, x2), diff(f, x3), diff(f, x4), diff(f, x5), diff(f, x6), diff(f, x7), diff(f, x8)];
gradFI_x = [diff(FI, x1), diff(FI, x2), diff(FI, x3), diff(FI, x4), diff(FI, x5), diff(FI, x6), diff(FI, x7), diff(FI, x8)];
gradf_l = [diff(f, l1), diff(f, l2)];

pp = -gradf_x.'*p - gradFI_x.'*mu - gradh_x.';
zero = gradf_l.'*p;

% wyznaczanie war. brzegowych
gradFI_x.'*(gradf_l.'*gradFI_x.')^-1*gradf_l.'
Sxf = [0;0;2*(xCells{end}(3) - xZad(end)); 2*(xCells{end}(4) - zZad(end)); 0;0;0;0];

pf = Sxf - gradFI_x.'*(gradf_l.'*gradFI_x.')^-1*gradf_l.'*Sxf

% podstawiam dane
time = Tend - tau;
idx = int16((time)/dt+1);
fraction = mod(Tend-tau, dt);

x = LinearApprox(xCells, idx, fraction);
lam = LinearApprox(lamCells, idx, fraction);

p1 = pVal(1); p2 = pVal(2); p3 = pVal(3); p4 = pVal(4); p5 = pVal(5); p6 = pVal(6); p7 = pVal(7); p8 = pVal(8); mu1 = pVal(9); mu2 = pVal(10);
x1 = x(1); x2 = x(2); x3 = x(3); x4 = x(4); x5 = x(5); x6 = x(6); x7 = x(7); x8 = x(8); l1 = lam(1); l2 = lam(2);
xZads = xZad(idx); zZads = zZad(idx); % TODO linear approx
mt = masses(1); mr = masses(2); m = masses(3);
dp1 = dpVal(1); dp2 = dpVal(2); dp3 = dpVal(3); dp4 = dpVal(4); dp5 = dpVal(5); dp6 = dpVal(6); dp7 = dpVal(7); dp8 = dpVal(8);

DiffPart = subs(dp) + subs(pp);
AlgebPart = subs(zero);
F = eval([DiffPart; AlgebPart]);

end

function xApprox = LinearApprox(x, idx, fraction)
xApprox = x{idx}*(1-fraction) + x{idx-1}*fraction;
end



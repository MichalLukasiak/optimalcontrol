%% program init
clc; clear; close all;
addpath(genpath('C:\Users\Michal\Desktop\mbs\src\'), 'C:\Users\Michal\Desktop\mbs\src\@ForwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@BackwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@Control');

%% model pick and its initial conditions
% model = Crane()


coords(1) = 4; % len
coords(2) = 0*pi; % angle


model = Crane(coords);
model.xOffsetTrajectory = 0;
model.yOffsetTrajectory = 0;

ForwardSimulator = ForwardSimulator(0, 0.5, 0.001);

%% classes init
[ masy, inercje, dlugosci, tlumienie ] = CraneOptCtrl();
model.EnterMassAndGeometryParameters(3, masy, inercje, dlugosci, tlumienie)

Control = Control(ForwardSimulator.SamplesAmount, model.CoordinatesAmount);
% Control.Apply(-981*ones(1,ForwardSimulator.SamplesAmount), 2);
BackwardSimulator = BackwardSimulator(ForwardSimulator.SamplesAmount);
ObjectiveFunction = ObjectiveFunction(model, ForwardSimulator, 2);
Plotter = Plotter();
Plotter.CreateControlPlot();
Printer = Printer();
Printer.CreateTempFolderAndLog();
AdaDeltaOptimizer = AdaDelta();

Printer.wypiszFunkcjeCelu(ObjectiveFunction, 2);
Plotter.ObjectiveFunctionFigure = figure();
stepStart = 150;
%% Loop
while(Plotter.StopCondition)
    [qAdjoint, vAdjoint] =  ForwardSimulator.Simulate(model, Control.u);
    [q, v] = ForwardSimulator.SimulateGGL_ode45(model, Control.u );
    [ err ] = CheckKellyForwardSimulationResults( q, qAdjoint, v, vAdjoint )
    
    [q, v, vp, M, C, Cq, Q, Gamma, A, B, lamAdj] = ForwardSimulator.CalculateMatrixesInTimeIntervals(model, Control.u );
    [lambda] = CalculateMatrixesInTimeIntervals_Crane(q, v, model, Control.u);
    x = cellfun(@vertcat, q, v, 'Uni', 0);
    [ grad ] = CalcGrad_Crane_Euler( x, lambda, ForwardSimulator, ObjectiveFunction, model );
    Control.ApplyNewObjectiveFunctionValue(ObjectiveFunction.Calculate(q, v))
    
    while(1)
        [uTemp, duTemp] = Control.OptimConstantStep(grad, model.IndexSterowan, stepStart);
        [q, v] = ForwardSimulator.Simulate(model, uTemp);
        JTemp = ObjectiveFunction.Calculate(q, v);
        if (JTemp > Control.J(end))
            stapStart = 0.6;
            break;
        else
            Control.ApplyNewU(uTemp, duTemp);
            Control.ApplyNewObjectiveFunctionValue(JTemp);
            if (IsObjectiveFunctionDecreasing(Control.J, 10))
                stepStart = stepStart*1.1; 
            end
        end
        Plotter.PlotObjectiveFunction(Control.J);
        Plotter.PlotControl(Control.u, model.IndexSterowan, ForwardSimulator.TEnd, ForwardSimulator.Dt);
        Printer.PodsumowanieIteracji(Control);
        
    end
    
        


    
%     Control.OptimConstantDecrease(grad, model.IndexSterowan, 5 )
%     

end

fmincon
model.animacjaRuchu(q, []);
% fclose(Printer.log);
% Printer.SaveFigure(Plotter.ControlFigure, 'sterowanie.png');

% zestawienie = Plotter.rysujZestawienieWykresow(q, v, Control, ForwardSimulator, ObjectiveFunction, model.BodiesAmount);
% Printer.SaveFigure(zestawienie, 'podsumowanie.png');
% Printer.MoveSimulationDataFolder(model, model.BodiesAmount, ForwardSimulator.TEnd, ForwardSimulator.Dt, Control.J(end), fi_start )

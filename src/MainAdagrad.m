%% program init
clc; clear; close all;
addpath(genpath('C:\Users\Michal\Desktop\mbs\src\'), 'C:\Users\Michal\Desktop\mbs\src\@ForwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@BackwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@Control');

%% model pick and its initial conditions
% model = Crane()


bodiesAmount = 5;
fi_start(1) = 0; % wartosc poczatkowa kata wahadla 
fi_start(2) = 0.1*pi;
fi_start(3) = 0.2*pi;
fi_start(4) = -0.05*pi;
fi_start(5) = 0.2*pi;

model = Pendulum(fi_start(1:bodiesAmount));


ForwardSimulator = ForwardSimulator(0, 3, 0.001);

%% classes init
[ masy, inercje, dlugosci, tlumienie ] = PendulumAdjontMbs();
model.EnterMassAndGeometryParameters(bodiesAmount, masy, inercje, dlugosci, tlumienie)
model.SetInitialConditions();
Control = Control(ForwardSimulator.SamplesAmount, model.CoordinatesAmount);
BackwardSimulator = BackwardSimulator(ForwardSimulator.SamplesAmount);
ObjectiveFunction = ObjectiveFunction(model, ForwardSimulator, bodiesAmount);
Plotter = Plotter();
Plotter.CreateControlPlot();
Plotter.CreateObjectiveFunctionFigure();
Printer = Printer();
Printer.CreateTempFolderAndLog();
% BFGS = BFGS();

Printer.wypiszFunkcjeCelu(ObjectiveFunction, model.BodiesAmount);
stepStart = 50;
%% Loop
while(Plotter.StopCondition)
    ForwardSimulator.Simulate(model, Control.u);
    [q, v, vp, M, C, Cq, Q, Gamma, A, B, lambda] = ForwardSimulator. CalculateMatrixesInTimeIntervals(model, Control.u );
    
   
    grad = BackwardSimulator.CalculateGrad(ForwardSimulator, ObjectiveFunction, model, Control.u, q, v, M, Cq, A, B);
    
    if (Control.Iteration <300)
    Control.ApplyNewObjectiveFunctionValue(ObjectiveFunction.Calculate(q, v));
    %%
    % % wyznaczanie wzmocnienia
    if length(Control.J) > 21
        if ( Control.J(end)> Control.J(end-1))
            stepStart = stepStart*0.6; % zmniejszenie kroku jesli f celu zwiekszyla sie
            Control.u = Control.uOld; % przywrocenie starego sterowania
            else if (IsObjectiveFunctionDecreasing(Control.J, 20))
               stepStart = stepStart*1.1;
            end
        end
    end
    %%
%     gradBFGS = -BFGS.CalcNew_du(Control.u, grad, model.IndexSterowan, model.CoordinatesAmount, []);
    [uNew, duNew] = Control.OptimConstantStep(grad, model.IndexSterowan, stepStart);
%     [uNew, duNew ] = Control.OptimConstantDecrease(grad, model.IndexSterowan, 1 );

    Control.ApplyNewU(uNew, duNew);
   else
        Control.AdaGrad(grad, model.IndexSterowan);
        [q, v] = ForwardSimulator.Simulate(model, Control.u);
        Control.ApplyNewObjectiveFunctionValue(ObjectiveFunction.Calculate(q, v));
    end

    
    Plotter.PlotObjectiveFunction(Control.J);
    Plotter.PlotControl(Control.u, model.IndexSterowan, ForwardSimulator.TEnd, ForwardSimulator.Dt);
    Printer.PodsumowanieIteracji(Control)
end
%% change control.u to the best one
Control.u = Control.uBest;
ForwardSimulator.Simulate(model, Control.u);
[q, v, vp, M, C, Cq, Q, Gamma, A, B, lambda] = ForwardSimulator. CalculateMatrixesInTimeIntervals(model, Control.u );


model.animacjaRuchu(q, Printer.SciezkaDoWynikowObecnejSymulacji);
fclose(Printer.log);
Printer.SaveFigure(Plotter.ControlFigure, 'sterowanie.png');

zestawienie = Plotter.rysujZestawienieWykresow_Pendulum(q, v, Control, ForwardSimulator, ObjectiveFunction, model.BodiesAmount);
Printer.SaveFigure(zestawienie, 'podsumowanie.png');
Printer.MoveSimulationDataFolder(model.Name, model.BodiesAmount, ForwardSimulator.TEnd, ForwardSimulator.Dt, Control.J(end), fi_start )

%% program init
clc; clear; close all;
addpath(genpath('C:\Users\Michal\Desktop\mbs\src\'), 'C:\Users\Michal\Desktop\mbs\src\@ForwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@BackwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@Control');

%% model pick and its initial conditions
coords(1) = 4; % len
coords(2) = 0*pi; % angle

model = Crane(coords);
ForwardSimulator = ForwardSimulator(0, 1, 0.001);

%% classes init
[ masy, inercje, dlugosci, tlumienie ] = CraneOptCtrl();
model.EnterMassAndGeometryParameters(3, masy, inercje, dlugosci, tlumienie)
model.SetInitialConditions();
Control = Control(ForwardSimulator.SamplesAmount, model.CoordinatesAmount);
Control.ChangeU(-981*ones(1,ForwardSimulator.SamplesAmount), 2);

BackwardSimulator = BackwardSimulator(ForwardSimulator.SamplesAmount);
ObjectiveFunction = ObjectiveFunction(model, ForwardSimulator, 2);
Plotter = Plotter();
Plotter.CreateControlPlot();
Printer = Printer();
Printer.CreateTempFolderAndLog();
AdaDeltaOptimizer = AdaDelta();

Printer.wypiszFunkcjeCelu(ObjectiveFunction, 2);
%% Loop
while(Plotter.StopCondition)
    [q, v] = ForwardSimulator.Simulate(model, Control.u);
    JTemp = ObjectiveFunction.Calculate(q, v);
    
    [q, v, vp, M, C, Cq, Q, Gamma, A, B, lambda] = ForwardSimulator. CalculateMatrixesInTimeIntervals(model, Control.u );
    grad = BackwardSimulator.CalculateGrad(ForwardSimulator, ObjectiveFunction, model, Control.u, q, v, M, Cq, A, B);
    
%    Control.OptimConstantStep(grad, model.IndexSterowan, 5);
    [uNew, duNew] = Control.OptimConstantDecrease(grad, model.IndexSterowan, 5 );
    Control.ApplyNewU(uNew, duNew);
    Control.ApplyNewObjectiveFunctionValue(JTemp);
    
    
%     Control.AdaDelta(grad, model.IndexSterowan);
    Plotter.PlotControl(Control.u, model.IndexSterowan);
    Printer.PodsumowanieIteracji(Control)
end
model.animacjaRuchu(q, []);
% fclose(Printer.log);
% Printer.SaveFigure(Plotter.ControlFigure, 'sterowanie.png');

% zestawienie = Plotter.rysujZestawienieWykresow(q, v, Control, ForwardSimulator, ObjectiveFunction, model.BodiesAmount);
% Printer.SaveFigure(zestawienie, 'podsumowanie.png');
% Printer.MoveSimulationDataFolder(model, model.BodiesAmount, ForwardSimulator.TEnd, ForwardSimulator.Dt, Control.J(end), fi_start )

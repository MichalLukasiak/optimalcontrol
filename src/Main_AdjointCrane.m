%% program init
clc; clear; close all;
addpath(genpath('C:\Users\Michal\Desktop\mbs\src\'), 'C:\Users\Michal\Desktop\mbs\src\@ForwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@BackwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@Control');

%% model pick and its initial conditions
% model = Crane()


coords(1) = 4; % len
coords(2) = 0*pi; % angle


model = Crane(coords);
model.xOffsetTrajectory = 5;
model.yOffsetTrajectory = 3;

ForwardSimulator = ForwardSimulator(0, 3, 0.001);

%% classes init
[ masy, inercje, dlugosci, tlumienie ] = CraneOptCtrl();
model.EnterMassAndGeometryParameters(3, masy, inercje, dlugosci, tlumienie)

Control = Control(ForwardSimulator.SamplesAmount, model.CoordinatesAmount);
% Control.Apply(-981*ones(1,ForwardSimulator.SamplesAmount), 2);
Control.SetInitialControl(-981*ones(1,ForwardSimulator.SamplesAmount), 2);
BackwardSimulator = BackwardSimulator(ForwardSimulator.SamplesAmount);
ObjectiveFunction = ObjectiveFunction(model, ForwardSimulator, 2);
Plotter = Plotter();
Plotter.CreateControlPlot();
Printer = Printer();
Printer.CreateTempFolderAndLog();
AdaDeltaOptimizer = AdaDelta();
% BFGS = BFGS();

Printer.wypiszFunkcjeCelu(ObjectiveFunction, 2);
Plotter.ObjectiveFunctionFigure = figure();
stepStart = 50;
%% Loop
while(Plotter.StopCondition)
%     [qAdjoint, vAdjoint] =  ForwardSimulator.Simulate(model, Control.u);
    [q, v] = ForwardSimulator.SimulateGGL_ode45(model, Control.u );
%     [ err ] = CheckKellyForwardSimulationResults( q, qAdjoint, v, vAdjoint )
    
%     [q, v, vp, M, C, Cq, Q, Gamma, A, B, lamAdj] = ForwardSimulator.CalculateMatrixesInTimeIntervals(model, Control.u );
    [lambda] = CalculateMatrixesInTimeIntervals_Crane(q, v, model, Control.u);
    x = cellfun(@vertcat, q, v, 'Uni', 0);
    [ grad ] = CalcGrad_Crane_Euler( x, lambda, ForwardSimulator, ObjectiveFunction, model );
%     gradAdj = BackwardSimulator.CalculateGrad(ForwardSimulator, ObjectiveFunction, model, Control.u, qAdjoint, vAdjoint, M, Cq, A, B);
    
    if (Control.Iteration < 3) %newton method for minimum
        [ stepOpt] = FindOptimalStep_Newton( grad, Control, model, ForwardSimulator, ObjectiveFunction);
         [uNew, duNew] = Control.OptimConstantStep(grad, model.IndexSterowan, stepOpt);

        Control.ApplyNewU(uNew, duNew);
        [q, v] = ForwardSimulator.Simulate(model, Control.u);
        Control.ApplyNewObjectiveFunctionValue(ObjectiveFunction.Calculate(q, v));
        
        Plotter.PlotObjectiveFunction(Control.J);
        Plotter.PlotControl(Control.u, model.IndexSterowan, ForwardSimulator.TEnd, ForwardSimulator.Dt);
        Printer.PodsumowanieIteracji(Control);
        continue;
    end

%     grad = gradAdj;
%     du_BFGS = BFGS.Calc_du(Control.u, grad);
%     grad =  du_BFGS;
    if (Control.Iteration < 200)
    if length(Control.J) > 21
        if ( Control.J(end)> Control.J(end-1))
            stepStart = stepStart*0.6; % zmniejszenie kroku jesli f celu zwiekszyla sie
            Control.u = Control.uOld; % przywrocenie starego sterowania
            else if (IsObjectiveFunctionDecreasing(Control.J, 20))
               stepStart = stepStart*1.1;
                 end
        end
    end
    
        [uNew, duNew] = Control.OptimConstantStep(grad, model.IndexSterowan, stepStart);
        
        Control.ApplyNewU(uNew, duNew);
        [q, v] = ForwardSimulator.Simulate(model, Control.u);
        Control.ApplyNewObjectiveFunctionValue(ObjectiveFunction.Calculate(q, v));
    else
        Control.AdaGrad(grad, model.IndexSterowan);
        [q, v] = ForwardSimulator.Simulate(model, Control.u);
        Control.ApplyNewObjectiveFunctionValue(ObjectiveFunction.Calculate(q, v));
    end
        

    Plotter.PlotObjectiveFunction(Control.J);
    Plotter.PlotControl(Control.u, model.IndexSterowan, ForwardSimulator.TEnd, ForwardSimulator.Dt);
    Printer.PodsumowanieIteracji(Control);
    
%     Control.OptimConstantDecrease(grad, model.IndexSterowan, 5 )
%   
    size(q)
    size(q{1})

end

% fmincon
model.animacjaRuchu(q, []);
% fclose(Printer.log);
% Printer.SaveFigure(Plotter.ControlFigure, 'sterowanie.png');

% zestawienie = Plotter.rysujZestawienieWykresow(q, v, Control, ForwardSimulator, ObjectiveFunction, model.BodiesAmount);
% Printer.SaveFigure(zestawienie, 'podsumowanie.png');
% Printer.MoveSimulationDataFolder(model, model.BodiesAmount, ForwardSimulator.TEnd, ForwardSimulator.Dt, Control.J(end), fi_start )

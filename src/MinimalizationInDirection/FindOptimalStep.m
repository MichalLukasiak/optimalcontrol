function [step ] = FindOptimalStep( grad, Control, model, ForwardSimulator, ObjectiveFunction)
display('Nowa minimalizacja w kierunku');

step = 1;
i = 2;
JTemps(1) = 1e10;
con = 1;
while(con)
    Control.Percentage = step;
    [uNews{i}, duNews{i}] = Control.OptimConstantDecrease(grad, model.IndexSterowan, 0 );
    [q, v] = ForwardSimulator.Simulate(model, uNews{i});
    JTemps(i) = ObjectiveFunction.Calculate(q, v);
    if (JTemps(i) < JTemps(i-1))
        step = step + 1;
    else
        step = step - 1.5;
    end
    if step < 0.3
        con = 0;
    end
    i = i + 1;
end

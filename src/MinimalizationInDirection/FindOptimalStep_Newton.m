function [ stepOpt] = FindOptimalStep_Newton( grad, Control, model, ForwardSimulator, ObjectiveFunction)
% calculate J and diff
% grad = grad / norm(grad);
    
JFirstDiff = 2;
stepOpt = 0;
ds = 2;
% ds = dstep;

while (JFirstDiff > 0.5)
    % wyznacz funkcje celu dla u, u - du, u + du
    du = -grad;
    for i = -1:1:1
        [q, v] = ForwardSimulator.Simulate(model, Control.CalculateU(du*(stepOpt + i * ds), model.IndexSterowan));
        JBasedOnMinusPlusDu(i+2) = ObjectiveFunction.Calculate(q, v);
    end
    JFirstDiff = (JBasedOnMinusPlusDu(3) - JBasedOnMinusPlusDu(1)) / (2*ds);
    JSecondDiff = (JBasedOnMinusPlusDu(3) - 2*JBasedOnMinusPlusDu(2) + JBasedOnMinusPlusDu(1)) / (ds^2);
    
    stepOpt = stepOpt - JFirstDiff/JSecondDiff;
end
end



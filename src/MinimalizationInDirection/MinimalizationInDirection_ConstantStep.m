function [ ] = MinimalizationInDirection_ConstantStep( grad, coords, Control, model, ForwardSimulator, ObjectiveFunction, Plotter, Printer)
isObjFunSmaller = 1;
display('Nowa minimalizacja w kierunku');
iter = 0;
while(isObjFunSmaller)
    [uNew, duNew] = Control.OptimConstantDecrease(grad, model.IndexSterowan, 3 );
%     [uNew, duNew ] = Control.OptimConstantStep(grad, model.IndexSterowan, 50 );
    [q, v] = ForwardSimulator.Simulate(model, uNew);
    JTemp = ObjectiveFunction.Calculate(q, v);
    
    isObjFunSmaller = Control.CompareObjectiveFunction(JTemp);
%     Control.CompareObjectiveFunction(JTemp);
    
%     if (isObjFunSmaller == 1) % keep change on u 
        Control.ApplyNewU(uNew, duNew);
        Control.ApplyNewObjectiveFunctionValue(JTemp);
        
        Plotter.PlotObjectiveFunction(Control.J);
        Plotter.PlotControl(Control.u, model.IndexSterowan);
        Printer.PodsumowanieIteracji(Control);
        iter = iter + 1;
        
%     end
    if (isObjFunSmaller == 0) % break min in direction
        break;
    end
%     if (iter == 0) 
%         Control.Percentage = Control.Percentage*0.8;
%         fprintf('iter = %d, percentage = %d\n', iter, Control.Percentage); 
%         isObjFunSmaller = 1;
%     else if (iter > 20)
%         Control.Percentage = Control.Percentage*1.2;
%         end
%     end
end
%% if first move didnt decresed J we look for minimum in half of step and 2x, 3x ets steps
% if (iter == 0)
%     oldPercentage = Control.Percentage;
% %     factors = [-0.9, 0.3, 1, 5, 10, 20, 50];
%     factors = -50:1:50;
%     i = 1;
%     for factor = factors
%         Control.Percentage = oldPercentage * factor;
%         [uNews{i}, duNews{i}] = Control.OptimConstantDecrease(grad, model.IndexSterowan, 0 );
%         [q, v] = ForwardSimulator.Simulate(model, uNews{i});
%         JTemps(i) = ObjectiveFunction.Calculate(q, v);
%         i = i + 1;
%     end
%     % look for minium J and assign u and du to Control
%     figure();
%     plot(factors, JTemps);
%     [JTemp, idxMin] = min(JTemps);
%     uNew = uNews{idxMin};
%     duNew = duNews{idxMin};
%     
%     Control.ApplyNewU(uNew, duNew);
%     Control.ApplyNewObjectiveFunctionValue(JTemp);
% 
%     Plotter.PlotObjectiveFunction(Control.J);
%     Plotter.PlotControl(Control.u, model.IndexSterowan);
%     Printer.PodsumowanieIteracji(Control);
% 
%     Control.Percentage = oldPercentage;
%     
%     fprintf('uzyto kroku nr %d\n', idxMin);
%     JTemps;
% 
% end



end

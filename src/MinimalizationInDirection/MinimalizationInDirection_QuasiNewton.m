function [ stepOpt] = MinimalizationInDirection_QuasiNewton( grad, coords, Control, model, ForwardSimulator, ObjectiveFunction, Plotter, Printer)
% calculate J and diff
grad = grad / norm(grad);
i = 1;
dstep = 1000;
steps = -dstep:dstep:10000;
for step = steps
    [uNew, duNew ] = Control.OptimConstantStep(grad, model.IndexSterowan, step );
    [q, v] = ForwardSimulator.Simulate(model, Control.CalculateU(-grad*step, model.IndexSterowan));
    JTemps(i) = ObjectiveFunction.Calculate(q, v);
    i = i + 1;
end

JdStep = diff(JTemps)/dstep;
J2dStep2 = diff(JdStep)/dstep;

figure();
plot(steps(1:length(JdStep)), JdStep);
title('J diff step');
figure();
plot(steps(1:length(J2dStep2)), J2dStep2);
title('J2 d2Step');
    
JFirstDiff = 1; krok = 1;
stepOpt = 0;
ds = 1e-2 / norm(grad);
% ds = dstep;
while( abs(JFirstDiff) > 1)
    % wyznacz funkcje celu dla u, u - du, u + du
    
    du = -grad;
    for i = -1:1:1
        [q, v] = ForwardSimulator.Simulate(model, Control.CalculateU(du*(stepOpt + i * ds), model.IndexSterowan));
        JBasedOnMinusPlusDu(i+2) = ObjectiveFunction.Calculate(q, v);
    end
    JFirstDiff = (JBasedOnMinusPlusDu(3) - JBasedOnMinusPlusDu(1)) / (2*ds);
    JSecondDiff = (JBasedOnMinusPlusDu(3) - 2*JBasedOnMinusPlusDu(2) + JBasedOnMinusPlusDu(1)) / (ds^2);
    
    stepOpt = stepOpt - JFirstDiff/JSecondDiff;
%     krok = norm(stepOpt*du);
    
%     Control.ApplyNewU(Control.CalculateU_plus_du( stepOpt*du, model.IndexSterowan), stepOpt*du);

    
%     [q, v] = ForwardSimulator.Simulate(model, Control.u);
%     JTemp = ObjectiveFunction.Calculate(q, v);
%     Control.ApplyNewObjectiveFunctionValue(JTemp);
%         


%     Plotter.PlotObjectiveFunction(Control.J);
%     Plotter.PlotControl(Control.u, model.IndexSterowan);
%     Printer.PodsumowanieIteracji(Control)
end
end



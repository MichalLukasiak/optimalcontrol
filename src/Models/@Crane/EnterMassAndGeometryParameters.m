function [] = EnterMassAndGeometryParameters( obj, bodiesAmount, bodiesMasses, bodiesInertias, bodiesLengths, dumpings )
%ENTERMASSANDGEOMETRYPARAMETERS Summary of this function goes here
%   Detailed explanation goes here
obj.BodiesAmount = bodiesAmount;
obj.ConstraintsAmount = 1;
obj.CoordinatesAmount = 4;
obj.BodiesMasses = bodiesMasses;
obj.BodiesLengths = bodiesLengths;
obj.BodiesInertias = bodiesInertias;
obj.Dumpings = dumpings;

end


function [  ] = EnterObjectiveFunctionParameters(obj, objectiveFunction, t0, tk, dt)
objectiveFunction.Dt = dt;
objectiveFunction.TStart = t0;
objectiveFunction.TEnd = tk;
lProbek = int16((tk -  t0)/dt) + 1;

objectiveFunction.fPodcalkowa.q.wsp = 200*[1,1]*(dt)/tk;
objectiveFunction.fPodcalkowa.q.wyk = [2,2];
% objectiveFunction.fPodcalkowa.q.zad{1} = TrajectoryGenerator( @Linear, 0, 0.2, lProbek);
% objectiveFunction.fPodcalkowa.q.zad{2} = TrajectoryGenerator( @Linear, 1, 0, lProbek);
% objectiveFunction.fPodcalkowa.q.zad{2} = 1;

[xd, yd] = TrajectoryCraneInvDynamics( t0, tk, lProbek, obj.xOffsetTrajectory, obj.yOffsetTrajectory );
objectiveFunction.fPodcalkowa.q.zad{1} = xd;
objectiveFunction.fPodcalkowa.q.zad{2} = yd;


objectiveFunction.fPodcalkowa.v.wsp = 0*[1,1]*(dt/tk);
objectiveFunction.fPodcalkowa.v.wyk = [2,2];
objectiveFunction.fPodcalkowa.v.zad{1} = 0;
objectiveFunction.fPodcalkowa.v.zad{2} = 0;



objectiveFunction.fPodcalkowa.u.wsp = 0*0.000005*[1,1]*dt;
objectiveFunction.fPodcalkowa.u.wyk = [2, 2];
objectiveFunction.fPodcalkowa.u.zad{1} = 0;
objectiveFunction.fPodcalkowa.u.zad{2} = 0;

objectiveFunction.fPodcalkowa.q.index = [3, 4];
objectiveFunction.fPodcalkowa.v.index = [3, 4];
objectiveFunction.fPodcalkowa.u.index = [1, 2];

objectiveFunction.fKoncowa.q.wsp = 0.01*[1,1];
objectiveFunction.fKoncowa.q.wyk = [2,2];
objectiveFunction.fKoncowa.q.zad = [obj.xOffsetTrajectory,4-obj.yOffsetTrajectory];


objectiveFunction.fKoncowa.v.wsp = 0.01*[1,1];
objectiveFunction.fKoncowa.v.wyk = [2,2];
objectiveFunction.fKoncowa.v.zad = [0,0];


objectiveFunction.fKoncowa.q.index = [3, 4];
objectiveFunction.fKoncowa.v.index = [3, 4];
end


function [ B ] = GetB( obj )
%oblicza macierz B = fvT
if (length(obj.B) < 1)
    obj.B = zeros(4,4);
end
    B = obj.B;
end



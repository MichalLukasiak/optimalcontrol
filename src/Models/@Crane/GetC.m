function C = GetC(obj)
q = obj.q;

C = (q(3)-q(1))^2 + q(4)^2 - q(2)^2;
end
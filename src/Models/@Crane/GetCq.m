function [ Cq ] = GetCq(obj)
q = obj.q;
Cq = 2*[q(1)-q(3), -q(2), q(3)-q(1), q(4)];
end


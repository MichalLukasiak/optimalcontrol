function [ CqLamqT ] = GetCqLambdaqT( obj, lam  )
%cqTlam qT po obliczniu analityczyn wynosi dla danego q, lamda
%transponuje na koncu
CqLamqT = 2 * lam*[...
    1 0 -1 0;
    0 -1 0 0;
    -1 0 1 0;
    -0 0 0 1];
    
    
end


function [ CqvqT ] = GetCqvqT(obj, q, v )
% oblicza  (Cq * v)q T
CqvqT = 2 * [...
    v(1) - v(3);
    -v(2);
    v(3) - v(1);
    v(4)];
    
end

function [ Rz ] = obliczRz( fi )
%oblicza macierz rotacji Rz dla danego fi
Rz = [cos(fi) -sin(fi);
    sin(fi) cos(fi)];
end


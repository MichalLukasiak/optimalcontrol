function [ FI_x ] = GetFI_x(obj, x)
%GETFI_X Summary of this function goes here
%   Detailed explanation goes here
FI_x = [...
 2*x(1) - 2*x(3), -2*x(2), 2*x(3) - 2*x(1), 2*x(4),       0,   0,       0,  0;
     x(5) - x(7),   -x(6),     x(7) - x(5),   x(8), x(1) - x(3), -x(2), x(3) - x(1), x(4)];

end


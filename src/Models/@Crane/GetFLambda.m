function [ F_lambda ] = GetFLambda(obj, x, BodiesMasses )
%GETFLAMBDA Summary of this function goes here
%   Detailed explanation goes here
mt = BodiesMasses(1);
mr = BodiesMasses(2);
m = BodiesMasses(3);
F_lambda = [...
             0, -(x(1) - x(3))/mt;
             0,         x(2)/mr;
             0,   (x(1) - x(3))/m;
             0,         -x(4)/m;
 -(x(1) - x(3))/mt,             0;
         x(2)/mr,             0;
   (x(1) - x(3))/m,             0;
         -x(4)/m,             0];

end


function [ gamma ] = GetGamma(obj)
q = obj.q;
v = obj.v;

gamma=-2*(v(1)^2-2*v(1)*v(3)-v(2)^2+v(3)^2+v(4)^2);

end



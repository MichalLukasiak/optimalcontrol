function [ M ] = GetM( obj )
if (length(obj.M)<1)
    diagVec = [obj.BodiesMasses(1), obj.BodiesMasses(2), obj.BodiesMasses(3), obj.BodiesMasses(3)];
    obj.M = diag(diagVec);
end
    M = obj.M;
end


function [ Q ] = GetQ(obj, uInMoment )
masy = obj.BodiesMasses;
tlumienie = obj.Dumpings;
v = obj.v;
q = obj.q;

Q=zeros(length(q),1);
g = 9.80665;

Q = [0;
    0;
    0;
    masy(3)*g];

Q = Q + uInMoment;
end



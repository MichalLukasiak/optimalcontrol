function [ f_x ] = Getf_x(obj, lambda, BodiesMasses)
mt = BodiesMasses(1);
mr = BodiesMasses(2);
m = BodiesMasses(3);
% oblicza pochodna f po x - crane
f_x = [...
-lambda(2)/mt,     0, lambda(2)/mt,     0, 1, 0, 0, 0;
      0, lambda(2)/mr,     0,     0, 0, 1, 0, 0;
   lambda(2)/m,     0, -lambda(2)/m,     0, 0, 0, 1, 0;
      0,     0,     0, -lambda(2)/m, 0, 0, 0, 1;
 -lambda(1)/mt,     0, lambda(1)/mt,     0, 0, 0, 0, 0;
      0, lambda(1)/mr,     0,     0, 0, 0, 0, 0;
   lambda(1)/m,     0, -lambda(1)/m,     0, 0, 0, 0, 0;
      0,     0,     0, -lambda(1)/m, 0, 0, 0, 0];

end


function fu = Getfu(obj)
fu = [...
    1 0;
    0 1;
    0 0;
    0 0];
end
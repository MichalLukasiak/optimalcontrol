function [  ] = SetInitialConditions(obj )
% coords1 - lenght, coords2 - angle
obj.q(1,1) = 0;
obj.q(2,1) = obj.InitialCoords(1); % length
obj.q(3,1) = obj.q(2)*sin(obj.InitialCoords(2));
obj.q(4,1) = obj.q(2)*cos(obj.InitialCoords(2));
obj.v = zeros(4,1);

end

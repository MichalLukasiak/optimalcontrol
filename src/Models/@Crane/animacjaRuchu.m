% rysuj animacje
function [] = animacjaRuchu(obj, q, filePath)
lProbek = length(q);

f1 = figure();
set(f1, 'PaperPositionMode','auto');
qMx = cell2mat(q);
xstart = min(min(qMx([1,3],:)))-0.1;
xend = max(max(qMx([1,3],:)))+0.1;
ystart = -max(max(qMx([4],:)))-0.1;
yend = 0.2;

figSizeScale = 200;
xlength = (xend-xstart)*figSizeScale;
ylength = (yend-ystart)*figSizeScale;
set(f1, 'Position', [10 10 xlength ylength])
wozek = plot([0,0], [0,0]); hold on;
wozek.LineWidth = 5;
wozek.Color = 'b';

rope = plot([0], [0]);
rope.LineWidth =3;
rope.Color = [1, 0, 1];

ball = scatter(0, 0);

axis ([xstart xend ystart yend])
for i=1:lProbek
    wozek.XData = [q{i}(1)-0.2; q{i}(1)+0.2];
    rope.XData = [q{i}(1); q{i}(3)];
    rope.YData = -[0; q{i}(4)];
    ball.XData = q{i}(3);
    ball.YData = -q{i}(4);
    
    pause(2e-3);
%     if (i == 1 || i == lProbek)
%         saveas(f1, strcat(filePath, num2str(i, '%06i')), 'png');
%     end
end

% drawnow
% frame = getframe(f1);
% im = frame2im(frame);
% [imind,cm] = rgb2ind(im,256);
% if i == 1;
%   imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
% else
%   imwrite(imind,cm,filename,'gif','WriteMode','append');
% end



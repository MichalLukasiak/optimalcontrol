function [] = EnterMassAndGeometryParameters( obj, bodiesAmount, bodiesMasses, bodiesInertias, bodiesLengths, dumpings )
%ENTERMASSANDGEOMETRYPARAMETERS Summary of this function goes here
%   Detailed explanation goes here
obj.BodiesAmount = bodiesAmount;
obj.ConstraintsAmount = 2*bodiesAmount;
obj.CoordinatesAmount = 3*bodiesAmount;
obj.BodiesMasses = bodiesMasses;
obj.BodiesLengths = bodiesLengths;
obj.BodiesInertias = bodiesInertias;
obj.Dumpings = dumpings;

end


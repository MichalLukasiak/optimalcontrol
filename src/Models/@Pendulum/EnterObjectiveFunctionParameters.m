function [  ] = EnterObjectiveFunctionParameters(obj, objectiveFunction, t0, tk, dt)
objectiveFunction.Dt = dt;
objectiveFunction.TStart = t0;
objectiveFunction.TEnd = tk;

objectiveFunction.fPodcalkowa.q.wsp = 1*[0,1,1,1,1];
objectiveFunction.fPodcalkowa.q.wyk = [2,2,2,2,2];
objectiveFunction.fPodcalkowa.q.zad{1} = 0;
% objectiveFunction.fPodcalkowa.q.zad{2} = generatorTrajektorii(lProbek, 0, pi);
objectiveFunction.fPodcalkowa.q.zad{2} = 0.1;
objectiveFunction.fPodcalkowa.q.zad{3} = pi;
objectiveFunction.fPodcalkowa.q.zad{4} = pi;
objectiveFunction.fPodcalkowa.q.zad{5} = pi;


objectiveFunction.fPodcalkowa.v.wsp = 0*[1,1,1,1,1]*(dt*tk);
objectiveFunction.fPodcalkowa.v.wyk = [2,2,2,2,2];
objectiveFunction.fPodcalkowa.v.zad{1} = 0;
objectiveFunction.fPodcalkowa.v.zad{2} = 0;
objectiveFunction.fPodcalkowa.v.zad{3} = 0;
objectiveFunction.fPodcalkowa.v.zad{4} = 0;
objectiveFunction.fPodcalkowa.v.zad{5} = 0;


objectiveFunction.fPodcalkowa.u.wsp = 0*0.000005*dt;
objectiveFunction.fPodcalkowa.u.wyk = [2];
objectiveFunction.fPodcalkowa.u.zad{1} = 0;

objectiveFunction.fPodcalkowa.q.index = [1, 6, 9, 12, 15];
objectiveFunction.fPodcalkowa.v.index = [1, 6, 9, 12, 15];
objectiveFunction.fPodcalkowa.u.index = [1, 6, 9, 12, 15];

objectiveFunction.fKoncowa.q.wsp = 0*1*[0,1,1,1,1];
objectiveFunction.fKoncowa.q.wyk = [2,2,2,2,2];
objectiveFunction.fKoncowa.q.zad = [0,pi,pi,pi,pi];


objectiveFunction.fKoncowa.v.wsp = 0*1*[1,1,1,1,1];
objectiveFunction.fKoncowa.v.wyk = [2,2,2,2,2];
objectiveFunction.fKoncowa.v.zad = [0,0,0,0,0];


objectiveFunction.fKoncowa.q.index = [1, 6, 9, 12, 15];
objectiveFunction.fKoncowa.v.index = [1, 6, 9, 12, 15];
end


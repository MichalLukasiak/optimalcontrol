function [ A ] = GetA( obj, lam )
% A = (Mvp)qT - fqT + (CqTlam)qT
% dwa pierwsze czlony = 0, bo M nie zalezy od q
% f - nie zalezy od q
%cqTlam qT po obliczniu analityczyn wynosi dla danego q, lamda
%transponuje na koncu

CqTlamqT = obj.GetCqLambdaqT(lam);

A=CqTlamqT;

end


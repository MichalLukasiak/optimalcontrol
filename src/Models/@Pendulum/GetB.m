function [ B ] = GetB( obj )
%oblicza macierz B = fvT
if (length(obj.B) < 1)
    lCzlonow = obj.BodiesAmount;
    tlumienie = obj.Dumpings;
    idx1 = zeros(1+2*(lCzlonow-1), 1);
    idx2 = zeros(1+2*(lCzlonow-1), 1);
    val = zeros(1+2*(lCzlonow-1), 1);

    idx1(1) = 1;
    idx2(1) = 1;
    val(1) = -tlumienie(1);
    iter = 2;
    for i=2:lCzlonow
        idx1(iter) = i*3; % tlumienie na czlon
        idx2(iter) = i*3;
        val(iter) = -tlumienie(i);
        iter = iter + 1;

        idx1(iter) = i*3-3; % reakcja na poprzedni czlon
        idx2(iter) = i*3;
        val(iter) = tlumienie(i);
        iter = iter + 1;
    end
    obj.B = sparse(idx1, idx2, val, lCzlonow*3, lCzlonow*3, 1+2*(lCzlonow-1))';
end
    B = obj.B;
end



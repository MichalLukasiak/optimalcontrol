function C = GetC(obj)
q = obj.q;
lCzlonow = length(obj.q)/3;
l = obj.BodiesLengths;

C = zeros(lCzlonow*2, 1);
%C(1:2,1)  = 0
C(1) = q(3); % fi1
C(2) = q(2); % y1
czynnikSinPoprzedni = 0;
czynnikCosPoprzedni = 0;

for i=2:lCzlonow
    czynnikSinAktualny = sin(q(3*i))*l(i)/2;
    czynnikCosAktualny = cos(q(3*i))*l(i)/2;
    
    C(2*i-1) = q(3*(i-1)-2) + czynnikSinPoprzedni - ( q(3*i-2) - czynnikSinAktualny);
    C(2*i) = q(3*(i-1)-1) -  czynnikCosPoprzedni - ( q(3*i-1) + czynnikCosAktualny);
                   
    czynnikSinPoprzedni = czynnikSinAktualny;
    czynnikCosPoprzedni = czynnikCosAktualny;
end
end
function [ Cq ] = GetCq(obj)
q = obj.q;
l = obj.BodiesLengths;
lCzlonow = length(q)/3;
idx1 = zeros(2+8*(lCzlonow-1),1);
idx2 = zeros(2+8*(lCzlonow-1),1);
val = zeros(2+8*(lCzlonow-1),1);

idx1(1) = 1; % pochodna po fi1
idx2(1) = 3;
val(1) = 1;

idx1(2) = 2;
idx2(2) = 2;
val(2) = -1; % niezerowy skladnik od wektora prosopadlego do osi ruchu

iter = 3; % index niezerowego elementu
for i=2:lCzlonow
    j=i-2; % nowy index pomocniczy
    idx1(iter) = 2*j+3;
    idx2(iter) = 3*j+1;
    val(iter) = 1; % piersza jedynka z macierzy jednostowej ( pochodna po r) 
    iter = iter + 1;
    idx1(iter) = 2*j+4;
    idx2(iter) = 3*j+2;
    val(iter) = 1;
    iter = iter + 1;

    idx1(iter) = 2*j+3; % pierwsza -1 z pochodnej po -r
    idx2(iter) = 3*j+4;
    val(iter) = -1;
    iter = iter + 1;
    idx1(iter) = 2*j+4; % druga -1
    idx2(iter) = 3*j+5;
    val(iter) = -1;
    iter = iter + 1;
    
    if ( i == 2 )
        pochodna_po_fi_i_minus_1_czynnik1 = cos(q(3*(i-1)))*l(i-1)/2;
        pochodna_po_fi_i_minus_1_czynnik2 = sin(q(3*(i-1)))*l(i-1)/2;
    else
        pochodna_po_fi_i_minus_1_czynnik1 = pochodna_po_fi_i_czynnik1;
        pochodna_po_fi_i_minus_1_czynnik2 = pochodna_po_fi_i_czynnik2;
    end
    
    pochodna_po_fi_i_czynnik1 = cos(q(3*i))*l(i)/2;
    pochodna_po_fi_i_czynnik2 = sin(q(3*i))*l(i)/2;
    
    idx1(iter) = 2*j+3; % pochodnej po fi(i-1)
    idx2(iter) = 3*j+3;
    val(iter) = pochodna_po_fi_i_minus_1_czynnik1;
    iter = iter + 1;
    idx1(iter) = 2*j+4; % pochodnej po fi(i-1) - czynnik drugi
    idx2(iter) = 3*j+3;
    val(iter) = pochodna_po_fi_i_minus_1_czynnik2;
    iter = iter + 1;
    
    idx1(iter) = 2*j+3; % pochodnej po fi(i) - czynnik pierwszy
    idx2(iter) = 3*j+6;
    val(iter) = pochodna_po_fi_i_czynnik1;
    iter = iter + 1;
    idx1(iter) = 2*j+4; % pochodnej po fi(i) - czynnik drugi
    idx2(iter) = 3*j+6;
    val(iter) = pochodna_po_fi_i_czynnik2;
    iter = iter + 1;
end

Cq = sparse(idx1, idx2, val, 2*lCzlonow, 3*lCzlonow, 2+8*(lCzlonow-1));
end


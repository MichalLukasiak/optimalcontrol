function [ CqLamqT ] = GetCqLambdaqT( obj, lam  )
%cqTlam qT po obliczniu analityczyn wynosi dla danego q, lamda
%transponuje na koncu
lWspolrzednych=length(obj.q);
lCzlonow = lWspolrzednych/3;
idx = zeros(lCzlonow-1,1);
values = zeros(lCzlonow-1,1);
l = obj.BodiesLengths;
% uwzgledniajac zera w wektorze Sb(i) i Sa(i) mo�na niezerwowe sk�adniki
% CqlamqT zapisa� jako
% czynnikSin - czynnik z sinusem (zawiera znak minus)
% czynnikCos - czynnik z cosinusem
for i=2:lCzlonow
    czynnikSin = -sin(obj.q(3*i))*l(i)/2;
    czynnikCos =  cos(obj.q(3*i))*l(i)/2;
    idx(i-1) = 3*i; % indexy 1 i 2 wartosci niezerwoych
    if (i~=lCzlonow)
        values(i-1) = czynnikSin*(lam(2*i-1) + lam(2*i+1)) + czynnikCos*(lam(2*i) + lam(2*i+2));
    else
        values(i-1) = czynnikSin*(lam(2*i-1)) + czynnikCos*(lam(2*i));
    end
end
CqLamqT = sparse(idx, idx, values, lWspolrzednych, lWspolrzednych);
    
end


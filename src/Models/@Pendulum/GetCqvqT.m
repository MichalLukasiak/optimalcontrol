function [ CqvqT ] = GetCqvqT(obj, q, v )
% oblicza  (Cq * v)q 
l = obj.BodiesLengths;
lCzlonow = length(v)/3;
for i=1:lCzlonow
    r(1:2, i) = q(3*i-2:3*i-1);
    fi(i) = q(3*i);
end
Cqvq = zeros(lCzlonow*2, lCzlonow*3);
% dwa pierwsze wersy zerowe
for i=2:lCzlonow
    Cqvq(2*i-1:2*i, 3*i-3) = -obliczRz(fi(i-1))*[0; -l(i-1)/2]*v(3*i-3);
    Cqvq(2*i-1:2*i, 3*i) = obliczRz(fi(i))*[0; l(i)/2]*v(3*i);
end
CqvqT = Cqvq';
end

function [ Rz ] = obliczRz( fi )
%oblicza macierz rotacji Rz dla danego fi
Rz = [cos(fi) -sin(fi);
    sin(fi) cos(fi)];
end


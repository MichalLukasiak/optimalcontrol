function [ gamma ] = GetGamma(obj)


q = obj.q;
v = obj.v;
l = obj.BodiesLengths;

gamma=zeros(length(q)*2/3,1);
lCzlonow = length(q)/3;

czynnikSinPoprzedni = 0; % czynnik zwiazany z sinusem - poprzedniego czlonu
czynnikCosPoprzedni = 0;

for i=2:lCzlonow
    czynnikSinAktualny = sin(q(3*i))*l(i)/2*v(3*i)^2; % czynnik zwizazany z sinusem - aktualnego (i-tego) czlonu
    czynnikCosAktualny = -cos(q(3*i))*l(i)/2*v(3*i)^2;
    
    gamma(2*i-1) = czynnikSinPoprzedni + czynnikSinAktualny;
    gamma(2*i) = czynnikCosPoprzedni + czynnikCosAktualny;
    
    czynnikSinPoprzedni = czynnikSinAktualny;
    czynnikCosPoprzedni = czynnikCosAktualny; 
end
end



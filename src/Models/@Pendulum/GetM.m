function [ M ] = GetM( obj )
if (length(obj.M)<1)
    diagVec =[];
    for i=1:obj.BodiesAmount
        diagVec = [diagVec; obj.BodiesMasses(i); obj.BodiesMasses(i); obj.BodiesInertias(i)];
    end
    obj.M = sparse(1:obj.CoordinatesAmount, 1:obj.CoordinatesAmount, diagVec, obj.CoordinatesAmount, obj.CoordinatesAmount, obj.CoordinatesAmount);
end
    M = obj.M;
end


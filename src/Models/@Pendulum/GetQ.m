function [ Q ] = GetQ(obj, uInMoment )
masy = obj.BodiesMasses;
tlumienie = obj.Dumpings;
v = obj.v;
q = obj.q;

Q=zeros(length(q),1);
g = 9.80665;

for i=1:length(q)/3
    Q(3*i-1) = -masy(i)*g;
end
Q(1) = Q(1) - tlumienie(1)*v(1);

for i=2:length(q)/3
    Q(3*i) = Q(3*i) - tlumienie(i)*v(3*i);
    Q(3*i-3) = Q(3*i-3) + tlumienie(i)*v(3*i);
end

Q = Q + uInMoment;
end



function [  ] = SetInitialConditions(obj )
obj.q(1:3,1) = [0;0;0];
fi_start = obj.fi_start;
for i=2:obj.BodiesAmount
    obj.q(i*3-2:i*3,1)=[ 
       obj.BodiesLengths(i)/2*sin(fi_start(i))+obj.q(3*i-5)+sin(obj.q(3*i-3))*obj.BodiesLengths(i-1)/2;
        -obj.BodiesLengths(i)/2*cos(fi_start(i))+obj.q(3*i-4)-cos(obj.q(3*i-3))*obj.BodiesLengths(i-1)/2;
        fi_start(i)];
end
obj.v = zeros(obj.BodiesAmount*3,1);
end

% rysuj animacje
function [] = animacjaRuchu(obj, q, filePath)
lProbek = length(q);
lCzlonow = obj.BodiesAmount;

f1 = figure();
set(f1, 'PaperPositionMode','auto');
xstart = min(cellfun(@(c) c(1), q))-0.1;
xend = max(cellfun(@(c) c(1), q))+0.1;
ystart = -1.2; yend = 1.2;

figSizeScale = 200;
xlength = (xend-xstart)*figSizeScale;
ylength = (yend-ystart)*figSizeScale;
set(f1, 'Position', [1 500 xlength ylength])
wozek = plot([0,0], [0,0]); hold on;
wozek.LineWidth = 5;
wozek.Color = 'b';
for wah=1:lCzlonow-1
    wahadlo{wah} = plot([0], [0]);
    wahadlo{wah}.LineWidth =3;
    wahadlo{wah}.Color = [mod(wah/3, 1), mod(wah/2, 1), mod(wah/4, 1)];
end
axis ([xstart xend ystart yend])
for i=1:lProbek
    wozek.XData = [q{i}(1)-0.2; q{i}(1)+0.2];
    for j=2:lCzlonow
        wahadlo{j-1}.XData = [q{i}(j*3-2)-obj.BodiesLengths(j)/2*sin(q{i}(j*3)), q{i}(j*3-2)+obj.BodiesLengths(j)/2*sin(q{i}(j*3))];
        wahadlo{j-1}.YData = [q{i}(j*3-1)+obj.BodiesLengths(j)/2*cos(q{i}(j*3)), q{i}(j*3-1)-obj.BodiesLengths(j)/2*cos(q{i}(j*3))];
    end
    pause(10e-3);
    if (i == 1 || i == lProbek)
        saveas(f1, strcat(filePath, num2str(i, '%06i')), 'png');
    end
end

% drawnow
% frame = getframe(f1);
% im = frame2im(frame);
% [imind,cm] = rgb2ind(im,256);
% if i == 1;
%   imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
% else
%   imwrite(imind,cm,filename,'gif','WriteMode','append');
% end



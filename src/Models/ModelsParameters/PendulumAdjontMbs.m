function [ masy, inercje, dlugosci, tlumienie ] = PendulumAdjontMbs()
parameters{1}.m = 2;
parameters{1}.J = 2;
parameters{1}.d = 0.1; % tlumienie wiskotyczne
% parameters{1}.d = 0; % tlumienie wiskotyczne
parameters{1}.l = 0;

parameters{2}.m = 0.876;
parameters{2}.l = 0.323;
parameters{2}.J = 0.0076;
parameters{2}.d = 0.215;
% parameters{2}.d = 0;
% parameters.czlon1.d = 0; % tlumienie wiskotyczne
% parameters.czlon2.d = 0;

parameters{3}.m = 0.938;
parameters{3}.l = 0.419;
parameters{3}.d = 0.002;
% parameters{3}.d = 0.0;
parameters{3}.J = 0.01372;

parameters{4}.m = 0.738;
parameters{4}.l = 0.319;
parameters{4}.d = 0.002;
% parameters{4}.d = 0.0;
parameters{4}.J = 0.01572;

parameters{5}.m = 0.638;
parameters{5}.l = 0.219;
parameters{5}.d = 0.002;
% parameters{4}.d = 0.0;
parameters{5}.J = 0.01172;

dlugosci = cellfun(@(c) c.l, parameters);
tlumienie = cellfun(@(c) c.d, parameters);
masy = cellfun(@(c) c.m, parameters);
inercje = cellfun(@(c) c.J, parameters);
end


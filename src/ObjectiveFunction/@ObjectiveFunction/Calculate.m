function [ J ] = Calculate(obj, q, v)
% oblicz wartosc funkcji celu
fPodcalkowa = obj.fPodcalkowa;
fKoncowa = obj.fKoncowa;

% J = v(1)^2 + q(6)-pi ^2 + qp(6)^2
fCelu = 0;
for i=1:obj.ConsideredVariables
    fCelu = fCelu + fKoncowa.q.wsp(i)*(q{end}(fKoncowa.q.index(i)) - fKoncowa.q.zad(i)) ^(fKoncowa.q.wyk(i));
    fCelu = fCelu + fKoncowa.v.wsp(i)*(v{end}(fKoncowa.v.index(i)) - fKoncowa.v.zad(i)) ^(fKoncowa.v.wyk(i));
    if ( fPodcalkowa.q.wsp(i) ~= 0)
        fCelu = fCelu + fPodcalkowa.q.wsp(i)*sum( ( cellfun(@(c) c(fPodcalkowa.q.index(i)), q) - fPodcalkowa.q.zad{i}).^(fPodcalkowa.q.wyk(i)));
    end
    if ( fPodcalkowa.v.wsp(i) ~= 0)
        fCelu = fCelu + fPodcalkowa.v.wsp(i)*sum((cellfun(@(c) c(fPodcalkowa.v.index(i)), v) - fPodcalkowa.v.zad{i}).^(fPodcalkowa.v.wyk(i)));
    end
end
J = fCelu;

end




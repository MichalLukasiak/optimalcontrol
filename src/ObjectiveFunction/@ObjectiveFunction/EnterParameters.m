function [  ] = EnterParameters( obj )
%ENTERPARAMETERS Summary of this function goes here
%   Detailed explanation goes here
obj.fPodcalkowa.q.wsp = 0*[0,1,1,1,1]*(dt)/(pi)^2/tk;
obj.fPodcalkowa.q.wyk = [2,2,2,2,2];
obj.fPodcalkowa.q.zad{1} = 0;
% obj.fPodcalkowa.q.zad{2} = generatorTrajektorii(lProbek, 0, pi);
obj.fPodcalkowa.q.zad{2} = pi;
obj.fPodcalkowa.q.zad{3} = pi;
obj.fPodcalkowa.q.zad{4} = pi;
obj.fPodcalkowa.q.zad{5} = pi;


obj.fPodcalkowa.v.wsp = 0*[1,1,1,1,1]*(dt*tk);
obj.fPodcalkowa.v.wyk = [2,2,2,2,2];
obj.fPodcalkowa.v.zad{1} = 0;
obj.fPodcalkowa.v.zad{2} = 0;
obj.fPodcalkowa.v.zad{3} = 0;
obj.fPodcalkowa.v.zad{4} = 0;
obj.fPodcalkowa.v.zad{5} = 0;


obj.fPodcalkowa.u.wsp = 0*0.000005*dt;
obj.fPodcalkowa.u.wyk = [2];
obj.fPodcalkowa.u.zad{1} = 0;

obj.fPodcalkowa.q.index = [1, 6, 9, 12, 15];
obj.fPodcalkowa.v.index = [1, 6, 9, 12, 15];
obj.fPodcalkowa.u.index = [1, 6, 9, 12, 15];

obj.fKoncowa.q.wsp = 1*1*[0,1,1,1,1];
obj.fKoncowa.q.wyk = [2,2,2,2,2];
obj.fKoncowa.q.zad = [0,pi,pi,pi,pi];


obj.fKoncowa.v.wsp = 0.1*1*[1,1,1,1,1];
obj.fKoncowa.v.wyk = [2,2,2,2,2];
obj.fKoncowa.v.zad = [0,0,0,0,0];


obj.fKoncowa.q.index = [1, 6, 9, 12, 15];
obj.fKoncowa.v.index = [1, 6, 9, 12, 15];

end


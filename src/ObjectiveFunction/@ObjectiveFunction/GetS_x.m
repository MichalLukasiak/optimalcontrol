function [ S_x ] = GetS_x( obj, x)
% oblicz pochodna czastkowa funkcji podcalkowej po x
wspolrzedne = x(1:4);
Sq = zeros(1, length(wspolrzedne));
for i=1:2
    Sq(1,obj.fKoncowa.q.index(i)) = obj.fKoncowa.q.wsp(i)*obj.fKoncowa.q.wyk(i)*(wspolrzedne(obj.fKoncowa.q.index(i))-obj.fKoncowa.q.zad(i))^(obj.fKoncowa.q.wyk(i)-1);
end


predkosci = x(5:8);
Sv = zeros(1, length(predkosci));
for i=1:2
    Sv(1,obj.fKoncowa.v.index(i)) = obj.fKoncowa.v.wsp(i)*obj.fKoncowa.v.wyk(i)*(predkosci(obj.fKoncowa.v.index(i))-obj.fKoncowa.v.zad(i))^(obj.fKoncowa.v.wyk(i)-1);
end

S_x = [Sq, Sv];
end

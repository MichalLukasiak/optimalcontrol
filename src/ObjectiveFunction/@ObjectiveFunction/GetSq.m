function [ Sq ] = GetSq( obj, consideredVariables, coordinates)
% oblicz pochodna czastkowa funkcji podcalkowej po polozeniach
wspolrzedne = coordinates;
Sq = zeros(1, length(wspolrzedne));
for i=1:consideredVariables
    Sq(1,obj.fKoncowa.q.index(i)) = obj.fKoncowa.q.wsp(i)*obj.fKoncowa.q.wyk(i)*(wspolrzedne(obj.fKoncowa.q.index(i))-obj.fKoncowa.q.zad(i))^(obj.fKoncowa.q.wyk(i)-1);
end
end

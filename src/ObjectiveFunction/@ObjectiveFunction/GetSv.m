function Sv = GetSv(obj, consideredVariables, velocitiesEnd)
% oblicz pochodna czastkowa funkcji podcalkowej po polozeniach
predkosci = velocitiesEnd;
Sv = zeros(1, length(predkosci));
for i=1:consideredVariables
    Sv(1,obj.fKoncowa.v.index(i)) = obj.fKoncowa.v.wsp(i)*obj.fKoncowa.v.wyk(i)*(predkosci(obj.fKoncowa.v.index(i))-obj.fKoncowa.v.zad(i))^(obj.fKoncowa.v.wyk(i)-1);
end
end


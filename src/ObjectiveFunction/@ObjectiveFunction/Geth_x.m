function [ h_x ] = Geth_x(obj, x , nrProbki)
wspolrzedne = x(1:4);
predkosci = x(5:8);
% oblicz pochodna czastkowa funkcji podcalkowej po polozeniach
hq = zeros(1, length(wspolrzedne));
for i=1:2
    if ( length(obj.fPodcalkowa.q.zad{i}) > 1)
        hq(1,obj.fPodcalkowa.q.index(i)) = obj.fPodcalkowa.q.wsp(i)*obj.fPodcalkowa.q.wyk(i)*(wspolrzedne(obj.fPodcalkowa.q.index(i))-obj.fPodcalkowa.q.zad{i}(nrProbki))^(obj.fPodcalkowa.q.wyk(i)-1);
    else
       hq(1,obj.fPodcalkowa.q.index(i)) = obj.fPodcalkowa.q.wsp(i)*obj.fPodcalkowa.q.wyk(i)*(wspolrzedne(obj.fPodcalkowa.q.index(i))-obj.fPodcalkowa.q.zad{i}(1))^(obj.fPodcalkowa.q.wyk(i)-1);
    
    end
end

% oblicz pochodna czastkowa funkcji podcalkowej po polozeniach
hv = zeros(1, length(predkosci));
for i=1:2
    if ( length( obj.fPodcalkowa.v.zad{i}) > 1)
        hv(1,obj.fPodcalkowa.v.index(i)) = obj.fPodcalkowa.v.wsp(i)*obj.fPodcalkowa.v.wyk(i)*(predkosci(obj.fPodcalkowa.v.index(i))-obj.fPodcalkowa.v.zad{i}(nrProbki))^(obj.fPodcalkowa.v.wyk(i)-1);
    else
        hv(1,obj.fPodcalkowa.v.index(i)) = obj.fPodcalkowa.v.wsp(i)*obj.fPodcalkowa.v.wyk(i)*(predkosci(obj.fPodcalkowa.v.index(i))-obj.fPodcalkowa.v.zad{i}(1))^(obj.fPodcalkowa.v.wyk(i)-1);
    end
end

h_x = [hq, hv];

end
function [ hq ] = Gethq( obj, consideredVariables, wspolrzedne, nrProbki)
% oblicz pochodna czastkowa funkcji podcalkowej po polozeniach
hq = zeros(1, length(wspolrzedne));
for i=1:consideredVariables
    if ( length(obj.fPodcalkowa.q.zad{i}) > 1)
        hq(1,obj.fPodcalkowa.q.index(i)) = obj.fPodcalkowa.q.wsp(i)*obj.fPodcalkowa.q.wyk(i)*(wspolrzedne(obj.fPodcalkowa.q.index(i))-obj.fPodcalkowa.q.zad{i}(nrProbki))^(obj.fPodcalkowa.q.wyk(i)-1);
    else
       hq(1,obj.fPodcalkowa.q.index(i)) = obj.fPodcalkowa.q.wsp(i)*obj.fPodcalkowa.q.wyk(i)*(wspolrzedne(obj.fPodcalkowa.q.index(i))-obj.fPodcalkowa.q.zad{i}(1))^(obj.fPodcalkowa.q.wyk(i)-1);
    
    end
end
end
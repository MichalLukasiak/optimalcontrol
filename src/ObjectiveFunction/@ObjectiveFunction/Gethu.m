function [ hu ] = Gethu( obj, uCells, lWspolrzednych, indexSterowan)
% oblicz pochodna czastkowa funkcji podcalkowej po polozeniach
lProbek = length(uCells);
    hu = zeros(length(indexSterowan), lProbek);
    for i=1:length(indexSterowan)
        uVec = cellfun(@(c) c(indexSterowan(i)), uCells)';
        hu(i,:)= obj.fPodcalkowa.u.wsp(i)*obj.fPodcalkowa.u.wyk(i)*uVec.^(obj.fPodcalkowa.u.wyk(i)-1);
    end
end


function [ hv ] = Gethv( obj, consideredVariables, wspolrzedne, nrProbki)
% oblicz pochodna czastkowa funkcji podcalkowej po polozeniach
hv = zeros(1, length(wspolrzedne));
for i=1:consideredVariables
    if ( length( obj.fPodcalkowa.v.zad{i}) > 1)
        hv(1,obj.fPodcalkowa.v.index(i)) = obj.fPodcalkowa.v.wsp(i)*obj.fPodcalkowa.v.wyk(i)*(wspolrzedne(obj.fPodcalkowa.v.index(i))-obj.fPodcalkowa.v.zad{i}(nrProbki))^(obj.fPodcalkowa.v.wyk(i)-1);
    else
        hv(1,obj.fPodcalkowa.v.index(i)) = obj.fPodcalkowa.v.wsp(i)*obj.fPodcalkowa.v.wyk(i)*(wspolrzedne(obj.fPodcalkowa.v.index(i))-obj.fPodcalkowa.v.zad{i}(1))^(obj.fPodcalkowa.v.wyk(i)-1);
    end
end
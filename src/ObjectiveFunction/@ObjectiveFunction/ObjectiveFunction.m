classdef ObjectiveFunction < handle
    % Untitled18 Add summary here
    %
    % NOTE: When renaming the class name Untitled18, the file name
    % and constructor name must be updated to use the class name.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object.
    
    properties
        % Public, tunable properties.

        fPodcalkowa;
        fKoncowa;
        
        TStart;
        TEnd;
        Dt;
        
        ConsideredVariables
    end
    
    properties (Nontunable)
        % Public, non-tunable properties.
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    properties (DiscreteState)
    end
    
    methods
        % Constructor
        function obj = ObjectiveFunction(aModel, forwardSimulator, consideredVariables)
            % Support name-value pair arguments when constructing the
            % object.
%             setProperties(obj,nargin,varargin{:});
            obj.ConsideredVariables = consideredVariables;
            aModel.EnterObjectiveFunctionParameters(obj, forwardSimulator.TStart, forwardSimulator.TEnd, forwardSimulator.Dt);
        end
        [] = EnterParameters( obj, aModel);
        Sv = GetSv( obj, bodiesAmount, velocitiesEnd);
        Sq = GetSq( obj, bodiesAmount, coordinates);
        hq = Gethq( obj, lCzlonow, wspolrzedne, nrProbki);
        hv = Gethv( obj, lCzlonow, wspolrzedne, nrProbki);
        [ hu ] = Gethu( obj, uCells, lWspolrzednych, indexSterowan)
        [ J ] = Calculate(obj, q, v);
        [ h_x ] = Geth_x(obj, x , nrProbki)
    end
    
    methods (Access = protected)
        %% Common functions
        function setupImpl(obj,u)
            % Implement tasks that need to be performed only once,
            % such as pre-computed constants.
        end
        
        function y = stepImpl(obj,u)
            % Implement algorithm. Calculate y as a function of
            % input u and discrete states.
            y = u;
        end
        
        function resetImpl(obj)
            % Initialize discrete-state properties.
        end
        
        %% Backup/restore functions
        function s = saveObjectImpl(obj)
            % Save private, protected, or state properties in a
            % structure s.
        end
        
        function loadObjectImpl(obj,s,wasLocked)
            % Read private, protected, or state properties from
            % the structure s and assign it to the object obj.
        end
        
        %% Advanced functions
        function validateInputsImpl(obj,u)
            % Validate inputs to the step method at initialization.
        end
        
        function z = getDiscreteStateImpl(obj)
            % Return structure of states with field names as
            % DiscreteState properties.
            z = struct([]);
        end
        
        function processTunedPropertiesImpl(obj)
            % Define actions to perform when one or more tunable property
            % values change.
        end
        
        function flag = isInputSizeLockedImpl(obj,index)
            % Set true when the input size is allowed to change while the
            % system is running.
            flag = false;
        end
        
        function flag = isInactivePropertyImpl(obj,prop)
            % Implement logic for making public properties invisible based on
            % object configuration, for the command line and block dialog.
        end
    end
end

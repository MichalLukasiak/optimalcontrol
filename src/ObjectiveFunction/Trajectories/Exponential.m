function [ trajektoria ] = Exponential(valStart, valEnd, lProbek )
logLast = 100;
trajektoria = log(linspace(1, logLast, lProbek))/log(logLast)*(valEnd-valStart) + valStart;
end

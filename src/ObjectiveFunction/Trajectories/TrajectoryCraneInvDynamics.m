function [ x, y ] = TrajectoryCraneInvDynamics( t0, tk, lProbek, xOffsetTrajectory, yOffsetTrajectory )
% trajektoria dla crane z AdjointMbs.pdf z �agodnym przypieszeniem i
% hamowaniem

t = linspace(t0, tk, lProbek);
tau = (- t + t0)/(t0 - tk);
x = xOffsetTrajectory*(70*tau.^9 - 315*tau.^8 + 540*tau.^7 - 420*tau.^6 + 126*tau.^5);
y = 4 - yOffsetTrajectory*(70*tau.^9 - 315*tau.^8 + 540*tau.^7 - 420*tau.^6 + 126*tau.^5);
% y = fliplr(y);
end


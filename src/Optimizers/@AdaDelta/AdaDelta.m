classdef AdaDelta < handle
    % Untitled Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.
    
    properties
        % Public, tunable properties.
        gamma = 0.9;
        RMS_u_iMinus1;
        RMS_grad;
    end
    
    properties (DiscreteState)
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    methods (Access = public)
        function du = Optimize(obj, du_i_1, grad)
            grad = grad / 1;
            eps = 1e-3;
            % initialize in first iterartion
            if isempty(obj.RMS_u_iMinus1)
                obj.RMS_u_iMinus1 = 0;
                obj.RMS_grad = 0.1*grad.^2;
            else
                obj.RMS_u_iMinus1 = obj.gamma*obj.RMS_u_iMinus1 + (1 - obj.gamma).*du_i_1.^2;
                obj.RMS_grad = obj.gamma*obj.RMS_grad + (1 - obj.gamma).*grad.^2;
            end
            du =  -  1.5*sqrt(obj.RMS_u_iMinus1 + eps).* grad ./ sqrt(obj.RMS_grad + eps);
            
            
        end
    end
    
    methods (Access = protected)
        
        
        function setupImpl(obj,u)
            % Implement tasks that need to be performed only once,
            % such as pre-computed constants.
        end
        
        function y = stepImpl(obj,u)
            % Implement algorithm. Calculate y as a function of
            % input u and discrete states.
            y = u;
        end
        
        function resetImpl(obj)
            % Initialize discrete-state properties.
        end
    end
end

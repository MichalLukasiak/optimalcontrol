classdef AdaGrad < handle
    % Untitled Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.
    
    properties
        % Public, tunable properties.
        G;
    end
    
    properties (DiscreteState)
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    methods (Access = public)
        function [ du ] = Optimize(obj, du_i_1, grad)
        % G - macierz sum gradientow - diagonalna
        if (isempty(obj.G))
            obj.G = grad*0;
        end
        %% aktualizacja sterowania
        obj.G = obj.G + grad.^2;
        learningRate = 1;
        
        du = - learningRate * grad ./ sqrt(obj.G + 1e-6);


        end 
    end
    
    methods (Access = protected)
        function setupImpl(obj,u)
            % Implement tasks that need to be performed only once,
            % such as pre-computed constants.
        end
        
        function y = stepImpl(obj,u)
            % Implement algorithm. Calculate y as a function of
            % input u and discrete states.
            y = u;
        end
        
        function resetImpl(obj)
            % Initialize discrete-state properties.
        end
    end
end

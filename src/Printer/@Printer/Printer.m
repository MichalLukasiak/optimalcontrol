classdef Printer < handle
    % Untitled Add summary here
    %
    % NOTE: When renaming the class name Untitled, the file name
    % and constructor name must be updated to use the class name.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object.
    
    properties
        % Public, tunable properties.
        log;
        FunkcjaCeluString;
        SciezkaDoWynikowObecnejSymulacji;
    end
    
    properties (Nontunable)
        % Public, non-tunable properties.
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    properties (DiscreteState)
    end
    
    methods
        % Constructor
        function obj = Printer(varargin)
            % Support name-value pair arguments when constructing the
            % object.
            %             setProperties(obj,nargin,varargin{:});
            
        end
        
        [ funkcjaCeluString ] = wypiszFunkcjeCelu( obj, objectiveFunction, lCzlonow );
        
        function [] = CreateTempFolderAndLog(obj)
            sciezkaDoWynikowObecnejSymulacji = 'C:\Users\Michal\Desktop\mbs\src\obecnaSymulacjaWyniki\';
            obj.SciezkaDoWynikowObecnejSymulacji = sciezkaDoWynikowObecnejSymulacji;
            if( exist(sciezkaDoWynikowObecnejSymulacji, 'dir') )
%                 rmdir( sciezkaDoWynikowObecnejSymulacji, 's' );
            end
            mkdir(sciezkaDoWynikowObecnejSymulacji);
            obj.log = fopen(strcat(sciezkaDoWynikowObecnejSymulacji, 'logTemp.txt'), 'at');
        end
        
        function [] = PodsumowanieIteracji(obj, control)
            iterPrint = strcat('-------- nr iteracji = ', num2str(control.Iteration), '--------------\n');
            fprintf(iterPrint);
            fprintf(obj.log, '');
            fprintf(obj.log, '%s', iterPrint);
            fprintf(obj.log, '');
            J = control.J(end);
            Krok = control.Krok(end);
            str = strcat('J = ', num2str(J), ', krok = ', num2str(Krok), '\n');
            fprintf(str); % wypisanie wartosci f. celu i wspolczynnika sterowania
        end
        
        function [] = MoveSimulationDataFolder(obj, modelName, lCzlonow, tk, dt, JEnd, fi_start )
            fileID = fopen('C:\Users\Michal\Desktop\mbs\solutions\lSym.txt','r');
            lSymulacji = fscanf(fileID,'%d');
            fclose(fileID);

            %% utworzenie odpowiednio nazwanego folderu w folderze solutions
            [ folderName ] = utworzNazweFolderuZWynikami( lSymulacji, lCzlonow, tk, dt, JEnd, fi_start, obj.FunkcjaCeluString, modelName );
            movefile('obecnaSymulacjaWyniki', folderName); % raname and move

            
            fileID = fopen('C:\Users\Michal\Desktop\mbs\solutions\lSym.txt','w');
            lSymulacji = lSymulacji+1;
            fprintf(fileID, '%d', lSymulacji);
            fclose(fileID);
        end
        
        function [] = SaveFigure(obj, h, name)
            saveas(h, strcat(obj.SciezkaDoWynikowObecnejSymulacji, name));
        end
        

        
        
        
    end
    
    methods (Access = protected)
        %% Common functions
        function setupImpl(obj,u)
            % Implement tasks that need to be performed only once,
            % such as pre-computed constants.
        end
        
        function y = stepImpl(obj,u)
            % Implement algorithm. Calculate y as a function of
            % input u and discrete states.
            y = u;
        end
        
        function resetImpl(obj)
            % Initialize discrete-state properties.
        end
        
        %% Backup/restore functions
        function s = saveObjectImpl(obj)
            % Save private, protected, or state properties in a
            % structure s.
        end
        
        function loadObjectImpl(obj,s,wasLocked)
            % Read private, protected, or state properties from
            % the structure s and assign it to the object obj.
        end
        
        %% Advanced functions
        function validateInputsImpl(obj,u)
            % Validate inputs to the step method at initialization.
        end
        
        function z = getDiscreteStateImpl(obj)
            % Return structure of states with field names as
            % DiscreteState properties.
            z = struct([]);
        end
        
        function processTunedPropertiesImpl(obj)
            % Define actions to perform when one or more tunable property
            % values change.
        end
        
        function flag = isInputSizeLockedImpl(obj,index)
            % Set true when the input size is allowed to change while the
            % system is running.
            flag = false;
        end
        
        function flag = isInactivePropertyImpl(obj,prop)
            % Implement logic for making public properties invisible based on
            % object configuration, for the command line and block dialog.
        end
    end
end

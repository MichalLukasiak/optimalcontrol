function [ funkcjaCeluString ] = wypiszFunkcjeCelu( obj, objectiveFunction, lCzlonow )
% wypisuje funckje celu w postaci: ...
fPodcalkowa = objectiveFunction.fPodcalkowa;
fKoncowa = objectiveFunction.fKoncowa;
fPodcalkowaq = '';
fPodcalkowav = '';
kosztKoncowyq = '';
kosztKoncowyv = '';
fPodcalkowau = '';
for i=1:lCzlonow
    fPodcalkowaq = ulozFunkcje('q', fPodcalkowa.q, i, fPodcalkowaq);
    fPodcalkowav = ulozFunkcje('v', fPodcalkowa.v, i, fPodcalkowav);
    kosztKoncowyq = ulozFunkcje('q', fKoncowa.q, i, kosztKoncowyq);
    kosztKoncowyv = ulozFunkcje('v', fKoncowa.v, i, kosztKoncowyv);
end
fPodcalkowau = ulozFunkcje('u', fPodcalkowa.u, 1, fPodcalkowau);

funkcjaCeluString = strcat('F. celu = int( ', fPodcalkowaq, fPodcalkowav, fPodcalkowau, ')dt ', kosztKoncowyq, kosztKoncowyv);
obj.FunkcjaCeluString = funkcjaCeluString;
fprintf('%s', funkcjaCeluString);
fprintf(obj.log, '%s', funkcjaCeluString);
end

function znak = wypiszZnak(val)
if val>= 0
    znak = ' + ';
else
    znak = ' - ';
end
end

function string = ulozFunkcje(qvString, struktura, i, string)
 if (struktura.wsp(i) ~= 0)
     if( iscell(struktura.zad))
         if ( length(struktura.zad{i}) > 1)
            string = strcat(string, wypiszZnak(struktura.wsp(i)), num2str(struktura.wsp(i)), '(', qvString, num2str(struktura.index(i)),  '+f(t)', ')^', num2str(struktura.wyk(i)));
         else
            string = strcat(string, wypiszZnak(struktura.wsp(i)), num2str(struktura.wsp(i)), '(', qvString, num2str(struktura.index(i)),  wypiszPlozenieZadane(struktura.zad{i}(1)), ')^', num2str(struktura.wyk(i)));
         end
     else
         string = strcat(string, wypiszZnak(struktura.wsp(i)), num2str(struktura.wsp(i)), '(', qvString, num2str(struktura.index(i)),  wypiszPlozenieZadane(struktura.zad(i)), ')^', num2str(struktura.wyk(i)));
     end
 end
end

function string = wypiszPlozenieZadane(polozenieZadane)
string = '';
if ( polozenieZadane > 0)
    string = strcat(' - ', num2str(polozenieZadane));
end
if ( polozenieZadane < 0)
    string = strcat(' + ', num2str(polozenieZadane));
end

end
    


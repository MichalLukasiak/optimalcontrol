function [  ] = DrawAnimation_Pendulum(q, v, tK, dt, nRows, nCols, BodiesLengths, BodiesAmount)
%% init
subplot = @(m,n,p) subtightplot (m, n, p, [0.01 0.01], [0.01 0.01], [0.01 0.01]);
lProbek = length(q);
subplotAmount = nRows*nCols;

%% create main window
mainFig = figure();
set(mainFig, 'PaperPositionMode','auto');
limits = FindAxisLimits(q, BodiesLengths, BodiesAmount);
xSpan = limits(2) - limits(1);
ySpan = limits(4) - limits(3);

figSizeScale = 900;
xlength = figSizeScale*xSpan*nCols/(ySpan*nRows);
ylength = figSizeScale;
set(mainFig, 'Position', [50 50 xlength ylength])

for row = 1:nRows
    for col = 1:nCols
        subplotIdx = (row-1)*nCols+col;
        [qInTime, time] = get_qInTime(subplotIdx, subplotAmount, q, tK, lProbek);
        DrawSnap(nRows, nCols, subplotIdx, qInTime, limits, BodiesAmount, BodiesLengths, time);


    end

end
end
function [] = DrawSnap(row, col, subplotIdx, qInTime, limits, BodiesAmount, BodiesLengths, time)
subplot = @(m,n,p) subtightplot (m, n, p, [0.01 0.01], [0.01 0.01], [0.01 0.01]);
    sp = subplot(row, col, subplotIdx);
    wozek = plot([0,0], [0,0]); hold on;
    wozek.LineWidth = 3;
    wozek.Color = 'b';
    plot([limits(1), limits(2)], [0,0], 'k'); % rail
    for wah=1:BodiesAmount-1
        wahadlo{wah} = plot([0, 0], [0, 0]);
        wahadlo{wah}.LineWidth =3;
        wahadlo{wah}.Color = [mod(wah/3, 1), mod(wah/2, 1), mod(wah/4, 1)];
    end
    
    wozek.XData = [qInTime(1)-0.1; qInTime(1)+0.1];
    for j=2:BodiesAmount
        wahadlo{j-1}.XData = [qInTime(j*3-2)-BodiesLengths(j)/2*sin(qInTime(j*3)), qInTime(j*3-2)+BodiesLengths(j)/2*sin(qInTime(j*3))];
        wahadlo{j-1}.YData = [qInTime(j*3-1)+BodiesLengths(j)/2*cos(qInTime(j*3)), qInTime(j*3-1)-BodiesLengths(j)/2*cos(qInTime(j*3))];
    end

    axis (limits)
    set(sp, 'box', 'on', 'Visible', 'on', 'xtick', [], 'ytick', [])
    timeText = text(0.02, 0.02, strcat(num2str(time, '%1.3f'), ' s'), 'Units', 'Normalized', 'VerticalAlignment', 'bottom', 'FontSize', 18);
    
end

function limits = FindAxisLimits(q, BodiesLengths, BodiesAmount)
qMx = cell2mat(q);
xMin = min(qMx(1,:))-0.1;
xMax = max(qMx(1,:))-0.1;
yMin = 0;
yMax = 0;

for i=2:BodiesAmount
    xMin = min(xMin, min(qMx(3*i-2,:)+BodiesLengths(i)/2));
    xMax = max(xMax, max(qMx(3*i-2,:)+BodiesLengths(i)/2));
    yMin = min(yMin, min(qMx(3*i-1,:)+BodiesLengths(i)/2));
    yMax = max(yMax, max(qMx(3*i-1,:)+BodiesLengths(i)/2));
end

limits = [xMin-0.05 xMax+0.05 yMin-0.05 yMax+0.05];
end

function [qInTime, time] = get_qInTime(subplotIdx, subplotAmount, q, tK, samplesAmount)
    frac = (subplotIdx-1)/(subplotAmount-1);
    sampleNr = int16(frac*samplesAmount);
    if (sampleNr == 0) sampleNr = 1; end
    time = frac*tK;
    qInTime = q{sampleNr};
end

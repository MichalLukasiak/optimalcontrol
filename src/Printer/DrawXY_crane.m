function [  ] = DrawXY_crane( q, dt, objectiveFunction )
% draws trajectory of xm and ym of crane mass
subplot = @(m,n,p) subtightplot (m, n, p, [0.1 0.1], [0.1 0.1], [0.1 0.1]);

xy = figure();
subplot(1,2,1);
plot(0:dt:length(q)*dt-dt, cellfun(@(c) c(3), q)); hold on; grid on;
% plot desired
plot(0:dt:length(q)*dt-dt ,objectiveFunction.fPodcalkowa.q.zad{1}, 'k--');
xlabel('czas [s]');
ylabel('xm [m]');
legend('uzyskana', 'zadana');



subplot(1,2,2);
plot(0:dt:length(q)*dt-dt, cellfun(@(c) c(4), q)); grid on; hold on;
plot(0:dt:length(q)*dt-dt, objectiveFunction.fPodcalkowa.q.zad{2}, 'k--');
xlabel('czas [s]');
ylabel('ym [m]');
end


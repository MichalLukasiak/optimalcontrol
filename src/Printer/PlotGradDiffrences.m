function [  ] = PlotGradDiffrences( gradK, gradAdj )
figure
subplot(1,2,1)
plot(gradK(2,:));hold on
plot(gradAdj(2,:)); hold on
legend('Kelley', 'Adjoint');
xlabel('czas [ms]');
ylabel('wartosc gradientu - wozek')

subplot(1,2,2)
plot(gradK(1,:));hold on
plot(gradAdj(1,:)); hold on
xlabel('czas [ms]');
ylabel('wartosc gradientu - korba')

end


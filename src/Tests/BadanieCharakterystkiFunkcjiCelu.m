%% program init
clc; clear; close all;
addpath(genpath('C:\Users\Michal\Desktop\mbs\src\'), 'C:\Users\Michal\Desktop\mbs\src\@ForwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@BackwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@Control');

%% model pick and its initial conditions
% model = Crane()


coords(1) = 4; % len
coords(2) = 0*pi; % angle

model = Crane(coords);
model.xOffsetTrajectory = 5;
model.yOffsetTrajectory = 3;
ForwardSimulator = ForwardSimulator(0, 3, 0.001);

%% classes init
[ masy, inercje, dlugosci, tlumienie ] = CraneOptCtrl();
model.EnterMassAndGeometryParameters(3, masy, inercje, dlugosci, tlumienie)

Control = Control(ForwardSimulator.SamplesAmount, model.CoordinatesAmount);
% Control.Apply(-981*ones(1,ForwardSimulator.SamplesAmount), 2);
BackwardSimulator = BackwardSimulator(ForwardSimulator.SamplesAmount);
ObjectiveFunction = ObjectiveFunction(model, ForwardSimulator, 2);
Plotter = Plotter();
Plotter.CreateControlPlot();
Printer = Printer();
Printer.CreateTempFolderAndLog();
AdaDeltaOptimizer = AdaDelta();

Printer.wypiszFunkcjeCelu(ObjectiveFunction, 2);
Plotter.ObjectiveFunctionFigure = figure();

%% Loop
while(Plotter.StopCondition)
    ForwardSimulator.Simulate(model, Control.u);
%     ForwardSimulator.SimulateGGL(model, Control.u);
    
    [q, v, vp, M, C, Cq, Q, Gamma, A, B, lambda] = ForwardSimulator. CalculateMatrixesInTimeIntervals(model, Control.u );
    grad = BackwardSimulator.CalculateGrad(ForwardSimulator, ObjectiveFunction, model, Control.u, q, v, M, Cq, A, B);
    
%     if (Control.Iteration < 1500)
%         grad = grad / norm(grad);
        MinimalizationInDirection_ConstantStep( grad, coords, Control, model, ForwardSimulator, ObjectiveFunction, Plotter, Printer)
%         stepOpt = MinimalizationInDirection_QuasiNewton( grad, coords, Control, model, ForwardSimulator, ObjectiveFunction, Plotter, Printer);
%         [~, stepOpt] = Control.Getdu_OptimConstantDecrease(grad, model.IndexSterowan, 1 );
%         [uNew, duNew ] = Control.OptimConstantDecrease(grad, model.IndexSterowan, 2 );
%         
%         Control.ApplyNewU(uNew, duNew);
%         [q, v] = ForwardSimulator.Simulate(model, Control.u);
%         Control.ApplyNewObjectiveFunctionValue(ObjectiveFunction.Calculate(q, v));
%     else
%         Control.AdaGrad(grad, model.IndexSterowan);
%         [q, v] = ForwardSimulator.Simulate(model, Control.u);
%         Control.ApplyNewObjectiveFunctionValue(ObjectiveFunction.Calculate(q, v));
%     end
        

    Plotter.PlotObjectiveFunction(Control.J);
    Plotter.PlotControl(Control.u, model.IndexSterowan);
    Printer.PodsumowanieIteracji(Control);
    
%     Control.OptimConstantDecrease(grad, model.IndexSterowan, 5 )
%     
pause(0.01);
end

model.animacjaRuchu(q, []);
fclose(Printer.log);
Printer.SaveFigure(Plotter.ControlFigure, 'sterowanie.png');

zestawienie = Plotter.rysujZestawienieWykresow(q, v, Control, ForwardSimulator, ObjectiveFunction, model.BodiesAmount);
Printer.SaveFigure(zestawienie, 'podsumowanie.png');
Printer.MoveSimulationDataFolder(model, model.BodiesAmount, ForwardSimulator.TEnd, ForwardSimulator.Dt, Control.J(end), fi_start )

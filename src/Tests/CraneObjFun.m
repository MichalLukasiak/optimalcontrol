function [ J ] = CraneObjFun( x )

addpath(genpath('C:\Users\Michal\Desktop\mbs\src\'), 'C:\Users\Michal\Desktop\mbs\src\@ForwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@BackwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@Control');

%% model pick and its initial conditions
model = Crane();

coords(1) = 1; % len
coords(2) = 0*pi; % angle

forwardSimulator = ForwardSimulator(0, 1, 0.001);
%% classes init
[ masy, inercje, dlugosci, tlumienie ] = CraneOptCtrl();
model.EnterMassAndGeometryParameters(3, masy, inercje, dlugosci, tlumienie)
model.SetInitialConditions(coords);
control = Control(forwardSimulator.SamplesAmount, model.CoordinatesAmount);
% BackwardSimulator = BackwardSimulator(ForwardSimulator.SamplesAmount);
objectiveFunction = ObjectiveFunction(model, forwardSimulator, 2);



model.SetInitialConditions(coords);
[q, v] = forwardSimulator.Simulate(model, x);

control.CompareObjectiveFunction(objectiveFunction.Calculate(q, v));
J = control.J(end);

end


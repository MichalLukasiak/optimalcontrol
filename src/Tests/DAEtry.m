function [] = DAEtry()
t0 = 0; tk = 1;
y0 = [1; -1];
yp0 = [2; 0];
[t, y] = ode15i(@odefun, [t0, tk], y0, yp0)
figure();
plot(t, y(:, 1));
hold on; grid on;
plot(t, y(:, 2));

end

function F = odefun(t, y, yp)
F = [...
    y(1) - yp(1) + 1;
    yp(1)*y(2) + 2];
end
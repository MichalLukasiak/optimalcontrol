% badanie trajektori TheOptCtrlAppRoachTodynamiceInverseProblem.pdf

t = linspace(0, 3, 1000);
tau = (-t - 0)/(0 - 3);
xd = 5*(70*tau.^9 - 315*tau.^8 + 540*tau.^7 - 420*tau.^6 + 126*tau.^5);
yd = 4 - 3*(70*tau.^9 - 315*tau.^8 + 540*tau.^7 - 420*tau.^6 + 126*tau.^5);

plot(xd, yd);
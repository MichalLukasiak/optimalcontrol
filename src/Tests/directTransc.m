%% program init
clc; clear; close all;
addpath(genpath('C:\Users\Michal\Desktop\mbs\src\'), 'C:\Users\Michal\Desktop\mbs\src\@ForwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@BackwardSimulator', 'C:\Users\Michal\Desktop\mbs\src\@Control');
opts = optimoptions(@fminunc,'Display','iter')
uopt = fminunc(@GetJ, zeros(2*51,1), opts)

figure
qAdjointMbs= cellfun(@(c) c(4), q);
plot(qAdjointMbs);
hold on;
tSpan = 0:0.001:0.5;
qReal = 4+0.5*100/110*9.80665*tSpan.^2;
err = norm(qAdjointMbs - qReal)
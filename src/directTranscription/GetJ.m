function [ J ] = GetJ( u )
%% program init
addpath(genpath('C:\Users\Michal\Desktop\mbs\src\'));
% fprintf('getJ');
%% model pick and its initial conditions
coords(1) = 4; % len
coords(2) = 0*pi; % angle

model = Crane(coords);
forwardSimulator = ForwardSimulator(0, 0.05, 0.001);

%% classes init
[ masy, inercje, dlugosci, tlumienie ] = CraneOptCtrl();
model.EnterMassAndGeometryParameters(3, masy, inercje, dlugosci, tlumienie)

objectiveFunction = ObjectiveFunction(model, forwardSimulator, 2);


    [q, v] = forwardSimulator.SimulateDirectTranscription(model, u);
    J = objectiveFunction.Calculate(q, v);
    



end

